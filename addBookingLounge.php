<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/RoomPrice.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$lounge = 1;
$roomDetails = getRoomPrice($conn, "WHERE type_id =?", array("type_id"), array($lounge), "i");


$conn->close();

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Add Booking | Cosiety" />
<title>Add Booking | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="grey-bg menu-distance2 same-padding overflow">
    <div class="divider"></div>


    <h1 class="backend-title-h1"><?php echo $roomDetails[0]->getType(); ?></h1>

    <div>

    <?php
    for ($cnt=0; $cnt < count($roomDetails) ; $cnt++) {
      if ($cnt % 2 == 0) {
        ?><form action="addBookingDetailsLounge.php" method="POST">
            <!-- <button class="two-box-div overflow hover-a booking-button"  onclick="window.location.href ='addBookingDetails.php';"> -->
            <button class="two-box-div overflow hover-a booking-button" type="submit" name="roomId" id="roomId" value="<?php echo $roomDetails[$cnt]->getId() ?>">
            <div class="color-header green-header">
              <img src="img/meeting-room2.png" class="header-icon" alt="Yearly Membership" title="Yearly Membership"> <p><?php echo $roomDetails[$cnt]->getName()  ?></p>
                </div>
                <div class="white-box-content booking-whitebox-content">
                    <p class="price-title"><b>RM<?php echo $roomDetails[$cnt]->getPrice() ?>/</b><?php echo $roomDetails[$cnt]->getDuration() ?></p>
                    <table class="details-table">
                      <tr>
                          <td>-</td>
                            <td> <?php echo $roomDetails[$cnt]->getDescription() ?></td>
                        </tr>
                    </table>
                </div>
            </button>
            </form><?php
          }else {
            ?><form action="addBookingDetailsLounge.php" method="POST">
                <!-- <button class="two-box-div overflow hover-a booking-button"  onclick="window.location.href ='addBookingDetails.php';"> -->
                <button class="two-box-div overflow second-box hover-a booking-button" type="submit" name="roomId" id="roomId" value="<?php echo $roomDetails[$cnt]->getId() ?>">
                <div class="color-header violet-header">
                  <img src="img/meeting-room2.png" class="header-icon" alt="Yearly Membership" title="Yearly Membership"> <p><?php echo $roomDetails[$cnt]->getName()  ?></p>
                    </div>
                    <div class="white-box-content booking-whitebox-content">
                        <p class="price-title"><b>RM<?php echo $roomDetails[$cnt]->getPrice() ?>/</b><?php echo $roomDetails[$cnt]->getDuration() ?></p>
                        <table class="details-table">
                          <tr>
                              <td>-</td>
                                <td> <?php echo $roomDetails[$cnt]->getDescription() ?></td>
                            </tr>
                        </table>
                    </div>
                </button>
                </form><?php
          }

    }
     ?>
    <!--<form action="locationWork.php" method="POST">-->

        <!--<form action="locationWorkHot.php" method="POST">-->
    <div class="clear"></div>
    <h1 class="backend-title-h1">Floor Plan</h1>
<<<<<<< HEAD
    <img src="img/zone2.jpg" alt="Floor Plan" title="Floor Plan" class="floorplan-img">
    <div class="divider"></div>
=======
       <a href="./img/zone1.jpg"  data-fancybox="images-preview" title="Floor Plan">
        <img src="img/zone1.png" class="floorplan-img" alt="Floor Plan" title="Click to Enlarge">
      </a>
    <div class="divider"></div>  
</div>
>>>>>>> master






</div>
<?php include 'js.php'; ?>
</body>
</html>
