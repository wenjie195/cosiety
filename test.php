<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Booking.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/RoomPrice.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    //$area_type = rewrite($_POST["area_type"]);
    $id = rewrite($_POST["roomId"]);
    // $duration = rewrite($_POST["duration"]);
    // $start_date = rewrite($_POST["start_date"]);
    // $total_ppl = rewrite($_POST["total_ppl"]);
    // $order_by = rewrite($_POST["order_by"]);
    //$discount = rewrite($_POST["discount"]);
    // $total_price = rewrite($_POST["total_price"]);
    // $total_seat = rewrite($_POST["total_seat"]);
    // $project_title = rewrite($_POST["project_title"]);
    // $project_details = rewrite($_POST["project_details"]);
}

//$conn->close();

$roomDetails = getRoomPrice($conn, "WHERE id = ?", array("id"), array($id), "i");
echo $roomDetails[0]->getPrice();
