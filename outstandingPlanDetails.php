<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Booking.php';
require_once dirname(__FILE__) . '/classes/BookingPrivate.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$bookingDetails = getBooking($conn);

$conn->close();

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Outstanding Plan | Cosiety" />
<title>Outstanding Plan | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">

    <form method="POST" id="idOfForm" onsubmit="doPreview(this.submited); return false;">

        <h1 class="receipt-title-h1">Booking Details</h1> <span class="logo-span"><img src="img/cosiety-logo.png" class="logo-img2" alt="Cosiety" title="Cosiety"></span>

        <div class="clear"></div>

        <?php
        if(isset($_POST['user_Booking_ID']))
        {
            $conn = connDB();
            $bookingDetails = getBooking($conn,"WHERE booking_id = ? ", array("booking_id") ,array($_POST['user_Booking_ID']),"s");
            $bookingUsername = $bookingDetails[0]->getOrderBy();
            $bookingUid = $bookingDetails[0]->getUid();

            $privateUserDetails = getPrivate($conn,"WHERE orderBy = ? ", array("orderBy") ,array($bookingUsername),"s");

            $privateUserUid = getUser($conn,"WHERE uid = ? ", array("uid") ,array($bookingUid),"s");
                if($bookingDetails != null)
                {
                ?>
                    <div class="receipt-half-div">
                    <p class="receipt-upper-p">Booking ID<br>
                        <b class="receipt-lower-p"><?php echo $bookingDetails[0]->getBookingId(); ?></b></p>
                    </div>     

                    <div class="clear"></div> 

                    <div class="receipt-half-div">
                    <p class="receipt-upper-p">Booking By<br>
                        <b class="receipt-lower-p"><?php echo $privateUserUid[0]->getFullName(); ?></b></p>
                    </div>    

                    <div class="clear"></div> 

                    <div class="receipt-half-div">
                        <p class="receipt-upper-p">Start Date<br>
                        <!-- <b class="receipt-lower-p">1/9/2019</b></p> -->
                        <b class="receipt-lower-p">
                            <?php $dateCreatedST = date("d/m/Y",strtotime($bookingDetails[0]->getStartDate()));echo $dateCreatedST;?>
                        </b>
                        </p>
                    </div>   

                    <div class="receipt-half-div second-receipt-half-div">
                        <p class="receipt-upper-p">End Date<br>
                        <!-- <b class="receipt-lower-p">1/10/2019</b></p> -->
                        <b class="receipt-lower-p">
                            <?php $dateCreatedEND = date("d/m/Y",strtotime($bookingDetails[0]->getEndDate()));echo $dateCreatedEND;?>
                        </b>
                    </div> 

                    <div class="clear"></div> 

                    <div class="receipt-half-div">
                    <p class="receipt-upper-p">Total Amount<br>
                        <b class="receipt-lower-p"><?php echo $bookingDetails[0]->getTotalPrice(); ?></b></p>
                    </div>     

                    <div class="clear"></div> 

                    <div class="receipt-half-div">
                    <p class="receipt-upper-p">Paid Amount<br>
                        <b class="receipt-lower-p"><?php echo $bookingDetails[0]->getPaymentAmount(); ?></b></p>
                    </div>  

                    <div class="clear"></div> 

                    <div class="receipt-half-div">
                    <p class="receipt-upper-p">Payment Date and Time<br>
                        <!-- <b class="receipt-lower-p"><?php //echo $privateUserDetails[0]->getPaymentTime(); ?></b></p> -->

                        <b class="receipt-lower-p">
                            <?php $dateCreatedST = date("d/m/Y",strtotime($privateUserDetails[0]->getPaymentTime()));echo $dateCreatedST;?>
                            <?php $dateCreatedT = date("H:m",strtotime($privateUserDetails[0]->getPaymentTime()));echo $dateCreatedT;?>
                        </b>
                    
                    </div> 

                    <div class="receipt-half-div">
                    <p class="receipt-upper-p">Payment References<br>
                        <b class="receipt-lower-p"><?php echo $bookingDetails[0]->getPaymentAmount(); ?></b></p>
                    </div> 

                    <input type="hidden" name="booking_id" id="booking_id" value="<?php echo $bookingDetails[0]->getBookingId() ?>">

                    <div class="clear"></div>
                    <div class="tempo-three-clear"></div>

                    <div class="width100 overflow receipt-two-btn-container">
                        <div class="fillup-2-btn-space"></div>
                            <input onclick="this.form.submited=this.value;"  type="submit" name="REJECT" value="REJECT" class="clean print-btn">
                            <input onclick="this.form.submited=this.value;"  type="submit" name="APPROVED" value="APPROVED" class="blue-btn payment-button clean next-btn view-plan-btn">
                        <div class="fillup-2-btn-space"></div>
                    </div>

                <?php
                }
            $conn->close();
        }
        else
        {    }
        ?>

    </form>

</div>

<?php include 'js.php'; ?>

<script type="text/javascript">
    function doPreview(buttonType)
    {
        switch(buttonType){
            case 'REJECT':
                form=document.getElementById('idOfForm');
                form.action='utilities/BookingrEJECTFunction.php';
                form.submit();
            break;
            case 'APPROVED':
                form=document.getElementById('idOfForm');
                form.action='utilities/BookingApprovedFunction.php';
                form.submit();
            break;
        }
        
    }
</script>

</body>
</html>