<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Booking.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/RoomPrice.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();


    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        $conn = connDB();
        $uid = $_SESSION['uid'];
        $name = rewrite($_POST['roomName']);
      }

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$bookingDetails = getBooking($conn);
$roomDetails = getRoomPrice($conn);

$conn->close();

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Outstanding Plan | Cosiety" />
<title>Outstanding Plan | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1 hover1 align-select-h1 issue-h1">Room</h1>

    <div class="clear"></div>
    <!-- <h2 class="backend-title-h2 review-title">Total: RM100.00</h2>       -->
    <div class="small-divider"></div>
    <form method="POST" action="utilities/editRoomFunction.php">
      <table class="details-table">
          <?php
              if(isset($_POST['roomName']))
              {
                  $conn = connDB();
                  $roomDetails = getRoomPrice($conn,"WHERE name = ? ", array("name") ,array($_POST['roomName']),"s");
              ?>

              <div class="shipping-input clean smaller-text2 fifty-input ow-mbtm">
                  <input class="shipping-input2 clean normal-input same-height-with-date" type="hidden" placeholder="Product Id" id="id" name="id" value="<?php echo $roomDetails[0]->getId() ?>">
              </div>
              <div class="shipping-input clean smaller-text2 fifty-input ow-mbtm">
                  <p>Name</p>
                  <input class="shipping-input2 clean normal-input same-height-with-date" type="text" placeholder="name" id="name" name="name" value="<?php echo $roomDetails[0]->getName() ?>">
              </div>
              <div class="shipping-input clean smaller-text2 fifty-input ow-mbtm">
                  <p>Price</p>
                  <input oninput="this.value = this.value.toUpperCase()" class="shipping-input2 clean normal-input same-height-with-date" type="number" placeholder="RM" id="price" name="price" value="<?php echo $roomDetails[0]->getPrice() ?>">
              </div>
              <div class="shipping-input clean smaller-text2 fifty-input ow-mbtm">
                  <p>Duration</p>
                  <input oninput="this.value = this.value.toUpperCase()" class="shipping-input2 clean normal-input same-height-with-date" type="text" placeholder="Duration" id="duration" name="duration" value="<?php echo $roomDetails[0]->getDuration() ?>">
              </div>
              <div class="shipping-input clean smaller-text2 fifty-input ow-mbtm">
                  <p>Discount</p>
                  <input oninput="this.value = this.value.toUpperCase()" class="shipping-input2 clean normal-input same-height-with-date" type="text" placeholder="Product Description" id="discount" name="discount" value="<?php echo $roomDetails[0]->getDiscount() ?>">
              </div>

              <div class="shipping-input clean smaller-text2 fifty-input ow-mbtm">
                  <p>Description</p>
                  <input oninput="this.value = this.value.toUpperCase()" class="shipping-input2 clean normal-input same-height-with-date" type="text" placeholder="Description" id="description" name="description" value="<?php echo $roomDetails[0]->getDescription() ?>">
              </div>
              <div class="shipping-input clean smaller-text2 fifty-input ow-mbtm">
                  <p>Display</p>
                  <select name= "display" type = "number">
                     <option value="1">Yes</option>
                     <option value="0">No</option>
                  </select>
              </div>
              <button type="submit" name="updateButton">Submit</button>

              <?php
              }
          ?>
      </table>
    </form>
</div>


<?php include 'js.php'; ?>
</body>
</html>
