<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Booking.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$bookingDetails = getBooking($conn);

$conn->close();

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Outstanding Plan | Cosiety" />
<title>Outstanding Plan | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1 hover1 align-select-h1 issue-h1">All Bookings</h1>
	
    <div class="clear"></div>
    <!-- <h2 class="backend-title-h2 review-title">Total: RM100.00</h2>       -->
    <div class="small-divider"></div>
    <div class="width100">
    	<div class="overflow-scroll-div">    
            <table class="issue-table">
            	<tr>
                	<thead>
                    	<th>No.</th>
                        <th>Booking ID</th>
                        <th>Name</th>
                        <th>Plan</th>
                        <th>Duration</th>
                        <th>Booking Status</th>
                        <th>Date</th>
                        <th>Action</th>
                    </thead>
                </tr>
                <tbody>

                <?php
                $conn = connDB();
                if($bookingDetails)
                {
                    for($cnt = 0;$cnt < count($bookingDetails) ;$cnt++)
                    {?>
                        <!-- <tr class="link-to-details"> -->
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $bookingDetails[$cnt]->getBookingId();?></td>

                            <td><?php $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($bookingDetails[$cnt]->getUid()),"s");
                                        echo $userDetails[0]->getUsername();?>
                                </td>

                            <td><?php echo $bookingDetails[$cnt]->getAreaType();?></td>
                            <td><?php echo $bookingDetails[$cnt]->getDuration();?></td>
                            <td><?php echo $bookingDetails[$cnt]->getPaymentVerify();?></td>
                            <td>
                                <?php $dateCreated = date("Y-m-d",strtotime($bookingDetails[$cnt]->getDateCreated()));echo $dateCreated;?>
                            </td>

                            <td>
                                <!-- <form action="#" method="POST">
                                    <button class="clean edit-anc-btn hover1" type="submit" name="order_id" value="<?php //echo $bookingDetails[$cnt]->getBookingId();?>">
                                        <img src="img/shipping1.png" class="edit-announcement-img hover1a" alt="Shipping Out" title="Shipping Out">
                                        <img src="img/shipping2.png" class="edit-announcement-img hover1b" alt="Shipping Out" title="Shipping Out">
                                    </button>
                                </form> -->

                                <form action="outstandingPlanDetails.php" method="POST" class="right-content-p">
                                    <button class="clean receipt-btn" type="submit" name="user_Booking_ID" value="<?php echo $bookingDetails[$cnt]->getBookingId();?>">
                                        <img src="img/receipt.png" class="hover-effect receipt-img" alt="Action" title="Action">
                                    </button>
                                </form>

                            </td>

                        </tr>
                        <?php
                    }
                }
                $conn->close();
                ?>
                </tbody>                 
            </table>
		</div>
    </div>     
</div>


<?php include 'js.php'; ?>
</body>
</html>