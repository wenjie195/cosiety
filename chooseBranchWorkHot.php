<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Booking.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $area_type = rewrite($_POST["area_type"]);
    $cost = rewrite($_POST["cost"]);
    $duration = rewrite($_POST["duration"]);
    $start_date = rewrite($_POST["start_date"]);
    $total_ppl = rewrite($_POST["total_ppl"]);
    $order_by = rewrite($_POST["order_by"]);
    $discount = rewrite($_POST["discount"]);
    $total_price = rewrite($_POST["total_price"]);
    $total_seat = rewrite($_POST["total_seat"]);
    $project_title = rewrite($_POST["project_title"]);
    $project_details = rewrite($_POST["project_details"]);
}

$conn->close();

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Choose a Branch | Cosiety" />
<title>Choose a Branch | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">

<!-- <form action="reserveSpace.php" method="POST"> -->

	<h1 class="backend-title-h1">Choose a Branch:</h1>
           
    <div class="three-div">
        <select class="three-select clean" id="" name="">
            <option value="Straits_Quay" name="Straits_Quay">Straits Quay</option>
            <option value="Suntech" name="Suntech">Suntech</option>
            <option value="One_Precinct" name="One_Precinct">One Precinct</option>
            <option value="Ixora_Hotel" name="Ixora_Hotel">Ixora Hotel</option>
            <option value="Menara_Suezcap" name="Menara_Suezcap">Menara Suezcap</option>
            <option value="One_Square" name="One_Square">One Square</option>
        </select>
    </div>
    <div class="divider"></div><div class="divider"></div>
    <div class="clear"></div>
    <div class="width100">
    	<table class="location-table">
        	<thead>
            	<th>No.</th>
                <th>Branch</th>
                <th>Distance</th>
                <th>Address</th>
            </thead>
            <tr>
            	<td>1.</td>
                <td>Straits Quay</td>
                <td>0.1km</td>
                <td>F17, Jalan Seri Tg Pinang 10470 Tanjung Tokong, Jalan Seri Tg Pinang, Seri Tanjung Pinang, 10470 Tanjung Tokong, Pulau Pinang</td>
            </tr>
            <tr>
            	<td>2.</td>
                <td>Suntech</td>
                <td>19.9km</td>
                <td>1-10-3, Lintang Mayang Pasir 1, Bandar Bayan Baru, 11950 Bayan Lepas, Pulau Pinang</td>
            </tr>   
            <tr>
            	<td>3.</td>
                <td>One Square</td>
                <td>19.9km</td>
                <td>2-1-23A One Square One World, Bayan Baru, 11950 Bayan Lepas, Penang</td>
            </tr>                   
            <tr>
            	<td>4.</td>
                <td>One Precinct</td>
                <td>20.5km</td>
                <td>Lengkok Mayang Pasir, Jalan Mayang Pasir1, Bandar Bayan Baru, 11950 Bayan Lepas, Penang</td>
            </tr>  
            <tr>
            	<td>5.</td>
                <td>Ixora Hotel</td>
                <td>27.6km</td>
                <td>3096 Jalan Baru, Bandar Perai Jaya, 13600 Perai, Pulau Pinang</td>
            </tr>   
            <tr>
            	<td>6.</td>
                <td>Menara SuezCap</td>
                <td>362km</td>
                <td>Menara SuezCap 1, 2, Jalan Kerinchi Kiri, Pantai Dalam, 59200 Kuala Lumpur, Federal Territory of Kuala Lumpur</td>
            </tr>                                               
        </table>
    
    </div>
    <div class="clear"></div>
    <div class="fillup-extra-space"></div><button class="blue-btn payment-button clean next-btn" onclick="window.location.href ='addBookingDetailsHot.php';">Next</button>
    <!-- <div class="fillup-extra-space"></div><a href="reserveSpace.php"><button class="blue-btn payment-button clean next-btn">Next</button></a> -->
    <div class="clear"></div> 
</form>
</div>


<?php include 'js.php'; ?>
</body>
</html>