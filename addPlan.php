<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Add Plan | Cosiety" />
<title>Add Plan | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1">Add Plan</h1>
	<div class="edit-half-div">
    	<p class="grey-text input-top-p">Plan Name</p>
        <input class="three-select clean" type="text" placeholder="Plan Name">
	</div>
	<div class="edit-half-div second-edit-half-div">
    	<p class="grey-text input-top-p">Category</p>
        <select class="three-select clean">
            	<option>Working Space (Single)</option>
                <option>Working Space (Group)</option>
                <option>Meeting Room</option>              
        </select>
	</div>            
	<div class="clear"></div>
	<div class="edit-half-div">
    	<p class="grey-text input-top-p">Min. Booking Duration (Month)</p>
        <input class="three-select clean" type="number" placeholder="1">
	</div>
	<div class="edit-half-div second-edit-half-div">
    	<p class="grey-text input-top-p">Price Per Min. Booking Duration (RM)</p>
        <input class="three-select clean" type="number" placeholder="120.00">
	</div>            
	<div class="clear"></div>
	<div class="edit-half-div">
    	<p class="grey-text input-top-p">Slot Type</p>
        <select class="three-select clean">
            	<option>Seat</option>
                <option>Small Meeting Room</option>
                <option>Big Meeting Room</option>               
        </select>
	</div>
	<div class="edit-half-div second-edit-half-div">
    	<p class="grey-text input-top-p">Can Choose the Slot?</p>
        <select class="three-select clean">
            	<option>Yes</option>
                <option>No</option>            
        </select>
	</div>     
    <div class="clear"></div>   
    <div class="width100 overflow">
    	<p class="grey-text input-top-p project-p">Description</p>
		<textarea class="clean width100 project-textarea" placeholder="Key in description here"></textarea>  
	</div>
    <div class="clear"></div>
    <div class="width100 overflow">
            <div class="upload-btn-wrapper">
              <button class="upload-btn">Upload Icon</button>
              <input class="hidden-input" type="file" name="myfile" />
            </div>
            <!-- Crop the image 16:9 -->
            <p class="img-preview">Image Preview</p> 
            <div class="left-img-preview"><img src="img/big-meeting-room.png" class="uploaded-img"></div><span class="right-remove-span hover-effect">Remove</span>           
    </div> 
    <div class="clear"></div> 
	<div class="divider"></div>
    <div class="fillup-extra-space"></div><a href="customisePlan.php"><button class="blue-btn payment-button clean next-btn">Confirm</button></a>
    <div class="clear"></div>
    <div class="fillup-extra-space2"></div><a  onclick="goBack()" class="cancel-a hover-effect">Cancel</a>
</div>


<?php include 'js.php'; ?>
</body>
</html>