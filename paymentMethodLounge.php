<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Booking.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/BookingWorkDesk.php';
require_once dirname(__FILE__) . '/classes/RoomPrice.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $id = rewrite($_POST["roomId"]);
    $area_type = rewrite($_POST["area_type"]);
    $title = rewrite($_POST["title"]);
    $duration = rewrite($_POST["duration"]);
    $start_date = rewrite($_POST["start_date"]);
    $discount = rewrite($_POST["discount"]);
    $total_price = rewrite($_POST["total_price"]);
    $totalPeople = rewrite($_POST["total_people"]);
    $timeLine = rewrite($_POST["timeline"]);
    // $project_title = rewrite($_POST["project_title"]);
    // $project_details = rewrite($_POST["project_details"]);

    //$ids = implode(",",$_POST["seat_id"]);






}

$roomDetails = getRoomPrice($conn, "WHERE id =?", array("id"), array($id), "i");

$conn->close();

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Payment Method | Cosiety" />
<title>Payment Method | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">

<!-- <form action="utilities/bookingFunction.php" method="POST"> -->
<form name= "checkboxLimited" action="utilities/bookingFunctionLounge.php" method="POST">

<h4> <?php echo $title?> - Straits Quay</h4>
<input type="hidden" name="title" value="<?php echo $title ?>">
<input type="hidden" name="total_people" value="<?php echo $totalPeople ?>">

<!-- <h4> <?php //echo $duration?> </h4>
<h4> <?php //echo $start_date?> </h4>
<h4> <?php //echo $discount?> </h4>
<h4> <?php //echo $total_price?> </h4>
<h4> <?php //echo $ids?> </h4>
<h4> <?php //echo $project_title?> </h4>
<h4> <?php //echo $project_details?> </h4> -->

<!-- <h4> <?php //echo $userDetails->getUsername();?> </h4> -->



	<h1 class="backend-title-h1">Payment</h1>
	<div class="three-div">
    	<p class="grey-text input-top-p">Duration</p>
        <p class="three-select-p"><?php echo $duration; ?> <?php echo $roomDetails[0]->getDuration() ?></p>
        <input type="hidden" name="duration" value="<?php echo $duration; ?>">
    </div>
    <!-- <div class="three-div">
      	<p class="grey-text input-top-p">Seat</p>
          <p class="three-select-p"><?php// echo $ids; ?></p>
          <input type="hidden" name="seat" value="<?php //echo $ids ?>">
      </div> -->
	<div class="three-div">
        <p class="grey-text input-top-p">Start Date</p>
        <p class="three-select-p"><?php echo $start_date ?></p>
        <input type="hidden" name="start_date" value="<?php echo $start_date ?>">
        <!-- <input type="date" class="three-select clean"> -->
    </div>
	<div class="three-div">
    	<p class="grey-text input-top-p">Discount</p>
        <p class="three-select-p"><?php echo $discount; ?>%</p>
        <input type="hidden" name="discount" id="discount" value="<?php echo $discount; ?>">
    </div>
        <div class="three-div">
          	<p class="grey-text input-top-p">Total Person</p>
              <p class="three-select-p"><?php echo $totalPeople; ?></p>
              <input type="hidden" name="total_people" id="total_people" value="<?php echo $totalPeople; ?>">
          </div>
    <div class="tempo-three-clear"></div>
 	<div class="three-div second-three-div">
    	<p class="grey-text input-top-p">Total</p>
        <p class="total-p">RM<?php $priceDiscount = $total_price * $duration * $totalPeople * ($discount/100);
                                    $totalPrice = $total_price * $duration * $totalPeople;
                                    echo $totalPrice - $priceDiscount;?></p>
        <input type="hidden" name="total_price" id="total_price" value="<?php $priceDiscount = $total_price * $duration * $totalPeople * ($discount/100);
                                    $totalPrice = $total_price * $duration * $totalPeople ;
                                    echo $totalPrice - $priceDiscount;?>">
        <input type="hidden" name="cost" id="cost" value="<?php echo $price = $total_price * $duration * $totalPeople ; ?>">
    </div>

    <div class="clear"></div>
<<<<<<< HEAD

=======
    <h2 class="backend-title-h2">Floor Plan</h2>
       <a href="./img/zone1.jpg"  data-fancybox="images-preview" title="Floor Plan">
        <img src="img/zone1.png" class="floorplan-img" alt="Floor Plan" title="Click to Enlarge">
      </a>
    <div class="clear"></div>

	<div class="divider"></div>
>>>>>>> master
    <h2 class="backend-title-h2">Choose Your Payment Method</h2>

    <div class="three-div">
<<<<<<< HEAD
        <select class="three-select clean" id="payment_method" name="payment_method" type = "text">
            <!-- <option value="ipay88" name="ipay88">ipay88</option> -->
            <!-- <option value="Visa or MasterCard" name="Visa or MasterCard">Visa or MasterCard</option> -->
            <option value="Online Banking" name="Online Banking">Online Banking</option>
=======
        <select class="three-select clean" id="payment_method" name="payment_method">
            <!-- <option value="ipay88" name="ipay88">ipay88</option>
            <option value="Visa_or_MasterCard" name="Visa_or_MasterCard">Visa_or_MasterCard</option> -->
            <option value="online banking" name="online_banking">Online Banking</option>
>>>>>>> master
        </select>
    </div>

    <div class="three-div">
      	<!-- <p class="grey-text input-top-p">Price (RM)</p> -->
          <input type="hidden" name="price" readonly value="<?php echo $total_price; ?>">
      </div>
      <div class="clear"></div>

      <div class="three-div">
        	<!-- <p class="grey-text input-top-p">Bank Reference :</p> -->
            <!-- <input type="text" name="bank_reference"> -->
            <input type="hidden" name="seat" readonly value="<?php echo $seat_id; ?>">
            <input type="hidden" name="date" readonly value="<?php echo $start_date; ?>">
            <input type="hidden" name="duration" value="<?php echo $duration; ?>">
            <input type="hidden" name="user" value="<?php echo $uid ?>">
            <input type="hidden" name="title" value="<?php echo $title ?>">
            <input type="hidden" name="timeline" value="<?php echo $timeLine ?>">
            <!-- <input type="hidden" name="seat_testing3" value="<?php //echo $seat_testing ?>"> -->
        </div>
      <div class="clear"></div>
  	<div class="divider"></div>

    <!-- <h2 class="backend-title-h2">Choose Your Payment Method</h2>
    <div class="three-div-radio">
        <label class="container2"><img src="img/ipay88.png" class="payment-img" alt="ipay88" title="ipay88">
          <input type="radio" id="ipay88" name="ipay88">
          <span class="checkmark2"></span>
        </label>
    </div>
    <div class="three-div-radio">
        <label class="container2"><img src="img/visa-mastercard.png" class="payment-img" alt="Visa/Master Card" title="Visa/Master Card">
          <input type="radio" id="Visa_or_MasterCard" name="Visa_or_MasterCard">
          <span class="checkmark2"></span>
        </label>
    </div>
    <div class="three-div-radio no-margin-right">
        <label class="container2">Online Banking
          <input type="radio" id="online_banking" name="online_banking">
          <span class="checkmark2"></span>
        </label>
    </div> -->

    <div class="divider"></div><div class="divider"></div>
    <div class="clear"></div>
    <div class="fillup-extra-space"></div><button class="blue-btn payment-button clean next-btn" type="submit" name="updateButtonLounge" >Pay</button>
    <!-- <div class="fillup-extra-space"></div><a href="reserveSpace.php"><button class="blue-btn payment-button clean next-btn">Next</button></a> -->
    <div class="clear"></div>
 	<div class="divider"></div><div class="divider"></div>
</form>
</div>

<?php include 'js.php'; ?>
</body>
</html>
