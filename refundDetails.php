<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Refund Details | Cosiety" />
<title>Refund Details | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
            <h1 class="receipt-title-h1">Refund Details</h1>
           
            <div class="clear"></div>
            <div class="receipt-half-div">
            	<p class="receipt-upper-p">Plan<br>
                <b class="receipt-lower-p">Co-Working Space (Hot Seat)</b></p>
            </div>            
            <div class="receipt-half-div second-receipt-half-div">
            	<p class="receipt-upper-p">Duration<br>
                <b class="receipt-lower-p">1 Month</b></p>
            </div> 
            <div class="clear"></div>  
            <div class="receipt-half-div">
            	<p class="receipt-upper-p">Start Date<br>
                <b class="receipt-lower-p">1/9/2019</b></p>
            </div>            
            <div class="receipt-half-div second-receipt-half-div">
            	<p class="receipt-upper-p">End Date<br>
                <b class="receipt-lower-p">1/10/2019</b></p>
            </div> 
            <div class="clear"></div> 
            <div class="receipt-half-div">
            	<p class="receipt-upper-p">Reserved Working Space<br>
                <b class="receipt-lower-p">5</b></p>
            </div>
            <div class="receipt-half-div second-receipt-half-div">
            	<p class="receipt-upper-p">by<br>
                <b class="receipt-lower-p">XXX Company</b></p>
            </div>                         
            <div class="clear"></div>
            <div class="receipt-half-div">
            	<p class="receipt-upper-p">Member Bank Name<br>
                <b class="receipt-lower-p">Tang Li Xin</b></p>
            </div>            
            <div class="receipt-half-div second-receipt-half-div">
            	<p class="receipt-upper-p">Bank<br>
                <b class="receipt-lower-p">Maybank</b></p>
            </div> 
            <div class="clear"></div>    
            <div class="receipt-half-div">
            	<p class="receipt-upper-p">Bank Account Number<br>
                <b class="receipt-lower-p">1128738718</b></p>
            </div>                     
            <div class="clear"></div> 
            <div class="width100 receipt-border"></div>               
            <div class="overflow width100 total-container">
            	<div class="receipt-left-total">Paid</div>
                <div class="receipt-right-total bigger-font">RM1995.00</div>
            </div>
            <div class="clear"></div>            
            <div class="overflow width100 total-container">
            	<div class="receipt-left-total">Cancel Discount</div>
                <div class="receipt-right-total slight-left">-</div>
            </div>
            <div class="clear"></div>             
            <div class="overflow width100 total-container">
            	<div class="receipt-left-total">Fine</div>
                <div class="receipt-right-total slight-left">-</div>
            </div>
            <div class="clear"></div> 
            <div class="overflow width100 total-container">
            	<div class="receipt-left-total bigger-font">Refund Amount</div>
                <div class="receipt-right-total bigger-font">RM1995.00</div>
            </div>
            <div class="clear"></div> 
            <div class="overflow width100 total-container">
            	<div class="receipt-left-total">Payment Method</div>
                <div class="receipt-right-total">Online Banking</div>
            </div>       
            <div class="clear"></div> 
            <div class="overflow width100 total-container">
            	<div class="receipt-left-total">Refund Date</div>
                <div class="receipt-right-total">12/6/2019   10:00 pm</div>
            </div>    
            <div class="clear"></div> 
            <div class="overflow width100 total-container padding-bottom-0">
            	<div class="receipt-left-total">Reason</div>
                <div class="receipt-right-total">The working space is under construction.</div>
            </div>                                   
            <div class="clear"></div> 
            <div class="width100 receipt-border"></div>             
            <div class="clear"></div>                                                                                                              
            <div class="divider"></div>
            <div class="clear"></div>
			<div class="fillup-extra-space"></div><button class="blue-btn payment-button clean" onclick="goBack()">Back</button>
            <div class="clear"></div>


</div>


<?php include 'js.php'; ?>
</body>
</html>