<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Booking.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $area_type = rewrite($_POST["area_type"]);
    $cost = rewrite($_POST["cost"]);
}


$conn->close();

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Booking Details (Seat) | Cosiety" />
<title>Booking Details (Room) | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
<form action="paymentMethod.php" method="POST">
<!-- <form action="utilities/bookingFunction.php" method="POST"> -->
<!-- <h4> <?php //echo $userDetails->getUsername();?> </h4> -->
<!-- <h4> <?php //echo $area_type?> </h4> -->
<!-- <h4> <?php //echo $cost?> </h4> -->

    <!-- <h1 class="backend-title-h1">Co-Working Space (Hot Seat)</h1> -->
    <h1 class="backend-title-h1">Private Suit - Straits Quay</h1>

    <input type="hidden" name="area_type" id="area_type" value="<?php echo $area_type?>">
    <input type="hidden" name="cost" id="cost" value="<?php echo $cost?>">


    <div class="clear"></div>
	<h2 class="backend-title-h2">Floor Plan</h2>    
    <img src="img/zone3.jpg" alt="Floor Plan" title="Floor Plan" class="floorplan-img">
    <div class="clear"></div>
    <div class="four-img-container">
        <div class="four-img-div">
            <a href="./img/working-space2.jpg"  data-fancybox="images-preview" title="Private Suit">
                <img src="img/working-space2.jpg" class="width100 opacity-hover" alt="Private Suit" title="Private Suit">
            </a>  
            <p class="four-img-p">Private Suit</p>  	
        </div>
        <div class="four-img-div middle-four-img-div2">
            <a href="./img/working-space4.jpg"  data-fancybox="images-preview" title="Private Suit">
                <img src="img/working-space4.jpg" class="width100 opacity-hover" alt="Private Suit" title="Private Suit">
            </a>  
            <p class="four-img-p">Private Suit</p>  	
        </div>                          
	</div>
	<h2 class="backend-title-h2">Choose your table</h2>    
    <div class="big-container-for-seat">
    	<div class="eight-checkbox">
            <label class="container1"> 1
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>
    	<div class="eight-checkbox">
            <label class="container1"> 2
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 3
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 4
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>
    	<div class="eight-checkbox">
            <label class="container1"> 5
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 6
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>
    	<div class="eight-checkbox">
            <label class="container1"> 7
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 8
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 9
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div> 
    	<div class="eight-checkbox">
            <label class="container1"> 10
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 11
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 12
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 13
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div> 
    	<div class="eight-checkbox">
            <label class="container1"> 14
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 15
              <input type="checkbox" disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 16
              <input type="checkbox" disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>   
    	<div class="eight-checkbox">
            <label class="container1"> 17
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>
    	<div class="eight-checkbox">
            <label class="container1"> 18
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 19
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 20
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>
    	<div class="eight-checkbox">
            <label class="container1"> 21
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 22
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>
    	<div class="eight-checkbox">
            <label class="container1"> 23
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 24
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 25
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div> 
    	<div class="eight-checkbox">
            <label class="container1"> 26
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 27
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 28
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 29
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div> 
    	<div class="eight-checkbox">
            <label class="container1"> 30
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 31
              <input type="checkbox" disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 32
              <input type="checkbox" disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>           
    	<div class="eight-checkbox">
            <label class="container1"> 133
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>
    	<div class="eight-checkbox">
            <label class="container1"> 34
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 35
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 36
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>
    	<div class="eight-checkbox">
            <label class="container1"> 37
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 38
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>
    	<div class="eight-checkbox">
            <label class="container1"> 39
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 40
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 41
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div> 
    	<div class="eight-checkbox">
            <label class="container1"> 42
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 43
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 44
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 45
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div> 
    	<div class="eight-checkbox">
            <label class="container1"> 46
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 47
              <input type="checkbox" disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 48
              <input type="checkbox" disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>   
    	<div class="eight-checkbox">
            <label class="container1"> 49
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>
    	<div class="eight-checkbox">
            <label class="container1"> 50
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 51
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 52
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>
    	<div class="eight-checkbox">
            <label class="container1"> 53
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 54
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>
    	<div class="eight-checkbox">
            <label class="container1"> 55
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 56
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 57
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div> 
    	<div class="eight-checkbox">
            <label class="container1"> 58
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 59
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 60
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 61
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div> 
    	<div class="eight-checkbox">
            <label class="container1"> 62
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 63
              <input type="checkbox" disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 64
              <input type="checkbox" disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>           
    	<div class="eight-checkbox">
            <label class="container1"> 65
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>
    	<div class="eight-checkbox">
            <label class="container1"> 66
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 67
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 68
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div> 
    	<div class="eight-checkbox">
            <label class="container1"> 69
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 70
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 71
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 72
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div> 
    	<div class="eight-checkbox">
            <label class="container1"> 73
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 74
              <input type="checkbox" disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>                 
                                                                                      
    </div>
    <div class="clear"></div>

	<p class="grey-text input-top-p">Project Title</p>
	<input type="text" class="three-select clean width100" placeholder="Key in Project Title" id="project_title" name="project_title">    
 	<p class="grey-text input-top-p project-p">Project Details</p>
    <textarea class="clean width100 project-textarea" placeholder="Key in Project Details" id="project_details" name="project_details"></textarea>  
       
    <!-- <div class="fillup-extra-space"></div><a href="paymentMethod.php"><button class="blue-btn payment-button clean">Proceed to Payment</button></a> -->

    <div class="fillup-extra-space"></div>
    <!-- <button input type="submit" name="submit" value="Submit" class="blue-btn payment-button clean">Proceed to Payment</button> -->
    <button input type="submit" name="submit" value="Submit" class="blue-btn payment-button clean">Submit</button>

    <div class="clear"></div>
    <div class="fillup-extra-space2"></div><a  onclick="goBack()" class="cancel-a hover-effect">Cancel</a>
</form>
</div>


<?php include 'js.php'; ?>
</body>
</html>