<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Analyse | Cosiety" />
<title>Analyse | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1">Analyse</h1>
    <div class="clear"></div>
 	<div class="small-divider width100"></div>
    <div class="clear"></div>
	<a href="analyseMember.php">
        <div class="four-white-div">
            <div class="color-circle color-circle1">
                <img src="img/new-member.png" class="circle-icon" alt="Members"  title="Members">
            </div>
            <p class="black-text white-div-title">Members</p>
            <p class="white-div-number">200</p>
        </div>
    </a>
	<a href="analyseRevenue.php">
        <div class="four-white-div second-white-div third-two four-two">
            <div class="color-circle color-circle2">
                <img src="img/revenue.png" class="circle-icon" alt="Revenue"  title="Revenue">
            </div>
            <p class="black-text white-div-title">Revenue (RM)</p>
            <p class="white-div-number">2,000.00</p>            
        </div>
    </a>   
	<a href="analyseBooking.php">
        <div class="four-white-div third-white-div">
            <div class="color-circle color-circle3">
                <img src="img/edit.png" class="circle-icon" alt="Bookings"  title="Bookings">
            </div>
            <p class="black-text white-div-title">Bookings</p>
            <p class="white-div-number">&nbsp;</p>  
        </div>
    </a>   
  
</div>


<?php include 'js.php'; ?>
</body>
</html>