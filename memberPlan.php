<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Number of Paid Plan | Cosiety" />
<title>Number of Paid Plan | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1 align-select-h1">Number of Paid Plan</a>
    </h1>
	<select class="clean align-h1-select">
    	<option>Latest</option>
        <option>Oldest</option>
     	<option>Highest Number</option>
        <option>Lowest Number</option>    
     	<option>Highest Amount</option>
        <option>Lowest Amount</option>             
    </select>
	<div class="clear"></div>
    <div class="width100 search-div overflow">
    	<div class="three-search-div">
        	<p class="upper-search-p">Member</p>
            <input class="search-input" type="text" placeholder="Member Name">
        </div>
    	<div class="three-search-div middle-three-search second-three-search">
        	<p class="upper-search-p">Company</p>
            <input class="search-input" type="text" placeholder="Company Name">
        </div>
        <div class="three-search-div">
			<button class="three-search blue-btn clean search-blue-btn">Search</button>
        </div>
               
    </div>
    <div class="clear"></div>    
    <div class="small-divider"></div>
    <div class="width100">
    	<div class="overflow-scroll-div">    
            <table class="issue-table">
            	<tr>
                	<thead>
                    	<th>No.</th>
                        <th>Member</th>
                        <th>Company</th>
                        <th>Position</th>
                        <th>Number of Paid Plan</th>
                        <th>Total Amount (RM)</th>                    
                    </thead>
                </tr>
                <tr data-url="profileDetails.php" class="link-to-details hover-effect">
                	<td>1.</td>
                    <td>Janice Lim</td>
                    <td>XXX Company</td>
                    <td>Employer</td>
                    <td>5</td>
                    <td>2000.00</td>                       
                </tr>
                <tr data-url="profileDetails.php" class="link-to-details hover-effect">
                	<td>2.</td>
                    <td>Alicia Lim</td>
                    <td>XXX Company</td>
                    <td>Employee</td>
                    <td>0</td>
                    <td>0</td>          
                </tr>                
            </table>
		</div>
    </div>
  		<!--
        <div class="clear"></div>
        <div class="fillup-leftspace"></div><a href="addBooking.php"><div class="blue-btn add-new-btn">Add New Booking</div></a>-->
  
     
</div>


<?php include 'js.php'; ?>
</body>
</html>