<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Booking.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/RoomPrice.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();


    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        $conn = connDB();
        $uid = $_SESSION['uid'];

        $id = md5(uniqid());
        $type = rewrite($_POST['type']);
        $name = rewrite($_POST['name']);
        $discount = rewrite($_POST["discount"]);
        $price = rewrite($_POST['price']);
        $duration = rewrite($_POST['duration']);
        $description = rewrite($_POST['description']);
      }

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$bookingDetails = getBooking($conn);
$roomDetails = getRoomPrice($conn);

$conn->close();

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Outstanding Plan | Cosiety" />
<title>Outstanding Plan | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1 hover1 align-select-h1 issue-h1">Room</h1>

    <div class="clear"></div>
    <!-- <h2 class="backend-title-h2 review-title">Total: RM100.00</h2>       -->
    <div class="small-divider"></div>
    <form method="POST" action="utilities/addRoomFunction.php">
	<div class="three-div">
    	<p class="grey-text input-top-p">Room Type</p>
           <select class="three-select clean"  type="text" name = "type">
             <option value="Lounge">Lounge</option>
             <option value="Work Desk">Work Desk</option>
             <option value="Private Suit">Private Suit</option>
             <option value="Meeting Room">Meeting Room</option>
          </select>
    </div>
	<div class="three-div middle-three-div second-three-div">
        <p class="grey-text input-top-p">Room Name</p>
        <input class="three-select clean" type="text" id="name" name="name" placeholder="Room Name">
        <!-- <input type="date" class="three-select clean"> -->
    </div>
	<div class="three-div">
    	<p class="grey-text input-top-p">Display</p>
          <select class="three-select clean" name= "display" type = "number">
             <option value="1">Yes</option>
             <option value="0">No</option>
          </select>
    </div>
    <div class="tempo-three-clear"></div>
 	<div class="three-div second-three-div">
    	<p class="grey-text input-top-p">Price (RM)</p>
        <input class="three-select clean" name="price" type="text" placeholder="RM">
    </div>
	<div class="three-div middle-three-div">
        <p class="grey-text input-top-p">Price Per</p>
          <select class="three-select clean" name= "duration" type = "text">
             <option value="Year">Year</option>
             <option value="Month">Month</option>
             <option value="Week">Week</option>
             <option value="Day">Day</option>
             <option value="Hour">Hour</option>
          </select>
    </div>
 	<div class="three-div second-three-div">
    	<p class="grey-text input-top-p">Discount (%)</p>
        <input class="three-select clean" type="text" id="discount" name="discount" placeholder="0">
    </div>
	<div class="clear"></div>
	<div class="width100 overflow">
    	<p class="grey-text input-top-p">Description</p>
		<textarea class="clean width100 project-textarea edit-margin-btm" name="description" placeholder="Description"></textarea> 
    </div>
    <div class="clear"></div>
	<div class="fillup-extra-space"></div><a href="profile.php"><button type="submit" id = "update" name = "update" class="blue-btn payment-button clean next-btn">Confirm</button></a>
    </form>
</div>
<!-- -->



<?php include 'js.php'; ?>
</body>
</html>
