<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Add Booking | Cosiety" />
<title>Add Booking | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1">Booking by</h1>
	<div class="width100 overflow">
    	<p class="grey-text input-top-p">Member</p>
        <select class="three-select clean">
        	<option>Alice Tang</option>
            <option>Felicia Tang</option>
        </select>
    </div>
    <div class="divider"></div>
	<h1 class="backend-title-h1">Working Space</h1>
    <div class="two-box-container">
        <button class="two-box-div overflow hover-a booking-button"  onclick="window.location.href ='adminAddBookingDetails.php';">
            <div class="color-header red-header">
                <img src="img/seat2.png" class="header-icon" alt="Dedicated Work Desk" title="Dedicated Work Desk"> <p>Dedicated Work Desk</p>
            </div>
            <div class="white-box-content booking-whitebox-content">
				<p class="price-title"><b>RM799.00/</b>month</p>
                <table class="details-table">
                	<tr>
                    	<td>-</td>
                        <td> Enjoy 20% off from the monthly rental</td>
                    </tr>                
                </table>
            </div>
        </button>
        <button class="two-box-div overflow second-box hover-a booking-button"  onclick="window.location.href ='adminAddBookingDetails.php';">
            <div class="color-header orange-header">
                <img src="img/seat.png" class="header-icon" alt="Co-Working Space (Hot Seat)" title="Co-Working Space (Hot Seat)"> <p>Co-Working Space (Hot Seat)</p>
            </div>
            <div class="white-box-content booking-whitebox-content">
				<p class="price-title"><b>RM180.00/</b>month</p>
                <table class="details-table">
                	<tr>
                    	<td>-</td>
                        <td> Enjoy 20% off from the monthly rental</td>
                    </tr>                   
                </table>
            </div>           
        </button> 
    </div>  

	<h1 class="backend-title-h1">Private Suit</h1>
    <div class="two-box-container">
        <button class="two-box-div overflow hover-a booking-button"  onclick="window.location.href ='adminAddBookingDetails.php';">
            <div class="color-header blue-header">
                <img src="img/group.png" class="header-icon" alt="1 Work Station" title="1 Work Station"> <p>1 Work Station</p>
            </div>
            <div class="white-box-content booking-whitebox-content">
				<p class="price-title"><b>RM1000.00/</b>month</p>
                <table class="details-table">
                	<tr>
                    	<td>-</td>
                        <td> Enjoy 20% off from the monthly rental</td>
                    </tr>
                   
                </table>
            </div>
        </button>
        <button class="two-box-div overflow second-box hover-a booking-button"  onclick="window.location.href ='adminAddBookingDetails.php';">
            <div class="color-header purple-header">
                <img src="img/group2.png" class="header-icon" alt="2 Work Stations" title="2 Work Stations"> <p>2 Work Stations</p>
            </div>
            <div class="white-box-content booking-whitebox-content">
				<p class="price-title"><b>RM1600.00/</b>month</p>
                <table class="details-table">
                	<tr>
                    	<td>-</td>
                        <td> Enjoy 20% off from the monthly rental</td>
                    </tr>                   
                </table>
            </div>           
        </button> 
    </div>  
    <div class="two-box-container">
        <button class="two-box-div overflow hover-a booking-button"  onclick="window.location.href ='adminAddBookingDetails.php';">
            <div class="color-header blue-header">
                <img src="img/group.png" class="header-icon" alt="3 Work Stations" title="3 Work Stations"> <p>3 Work Stations</p>
            </div>
            <div class="white-box-content booking-whitebox-content">
				<p class="price-title"><b>RM2400.00/</b>month</p>
                <table class="details-table">
                	<tr>
                    	<td>-</td>
                        <td> Enjoy 20% off from the monthly rental</td>
                    </tr>
                   
                </table>
            </div>
        </button>
        <button class="two-box-div overflow second-box hover-a booking-button"  onclick="window.location.href ='adminAddBookingDetails.php';">
            <div class="color-header purple-header">
                <img src="img/group2.png" class="header-icon" alt="4 Work Stations" title="4 Work Stations"> <p>4 Work Stations</p>
            </div>
            <div class="white-box-content booking-whitebox-content">
				<p class="price-title"><b>RM3200.00/</b>month</p>
                <table class="details-table">
                	<tr>
                    	<td>-</td>
                        <td> Enjoy 20% off from the monthly rental</td>
                    </tr>                   
                </table>
            </div>           
        </button> 
    </div>  
    <div class="two-box-container hover-a"  onclick="window.location.href ='adminAddBookingDetails.php';">
        <button class="two-box-div overflow booking-button">
            <div class="color-header blue-header">
                <img src="img/group.png" class="header-icon" alt="5 Work Stations" title="5 Work Stations"> <p>5 Work Stations</p>
            </div>
            <div class="white-box-content booking-whitebox-content">
				<p class="price-title"><b>RM4000.00/</b>month</p>
                <table class="details-table">
                	<tr>
                    	<td>-</td>
                        <td> Enjoy 20% off from the monthly rental</td>
                    </tr>
                   
                </table>
            </div>
        </button>
        <button class="two-box-div overflow second-box hover-a booking-button"  onclick="window.location.href ='adminAddBookingDetails.php';">
            <div class="color-header purple-header">
                <img src="img/group2.png" class="header-icon" alt="6 Work Stations" title="6 Work Stations"> <p>6 Work Stations</p>
            </div>
            <div class="white-box-content booking-whitebox-content">
				<p class="price-title"><b>RM4800.00/</b>month</p>
                <table class="details-table">
                	<tr>
                    	<td>-</td>
                        <td> Enjoy 20% off from the monthly rental</td>
                    </tr>                   
                </table>
            </div>           
        </button> 
    </div>  
    <div class="two-box-container">
        <button class="two-box-div overflow hover-a booking-button"  onclick="window.location.href ='adminAddBookingDetails.php';">
            <div class="color-header blue-header">
                <img src="img/group.png" class="header-icon" alt="7 Work Stations" title="7 Work Stations"> <p>7 Work Stations</p>
            </div>
            <div class="white-box-content booking-whitebox-content">
				<p class="price-title"><b>RM5600.00/</b>month</p>
                <table class="details-table">
                	<tr>
                    	<td>-</td>
                        <td> Enjoy 20% off from the monthly rental</td>
                    </tr>
                   
                </table>
            </div>
        </button>
        <button class="two-box-div overflow second-box hover-a booking-button"  onclick="window.location.href ='adminAddBookingDetails.php';">
            <div class="color-header purple-header">
                <img src="img/group2.png" class="header-icon" alt="8 Work Stations" title="8 Work Stations"> <p>8 Work Stations</p>
            </div>
            <div class="white-box-content booking-whitebox-content">
				<p class="price-title"><b>RM6400.00/</b>month</p>
                <table class="details-table">
                	<tr>
                    	<td>-</td>
                        <td> Enjoy 20% off from the monthly rental</td>
                    </tr>                   
                </table>
            </div>           
        </button> 
    </div> 



	<h1 class="backend-title-h1">Lounge</h1>
    <div class="two-box-container">
        <button class="two-box-div overflow hover-a booking-button"  onclick="window.location.href ='adminAddBookingDetails.php';">
            <div class="color-header green-header">
                <img src="img/meeting-room2.png" class="header-icon" alt="Yearly Membership" title="Yearly Membership"> <p>Yearly Membership</p>
            </div>
            <div class="white-box-content booking-whitebox-content">
				<p class="price-title"><b>RM1999.00/</b>year</p>
                <table class="details-table">
                	<tr>
                    	<td>-</td>
                        <td> RM999 only if sign up on Nov 2019</td>
                    </tr>                 
                </table>
            </div>
        </button>
        <button class="two-box-div overflow second-box hover-a booking-button"  onclick="window.location.href ='adminAddBookingDetails.php';">
            <div class="color-header violet-header">
                <img src="img/meeting-room.png" class="header-icon" alt="Monthly Membership" title="Monthly Membership"> <p>Monthly Membership</p>
            </div>
            <div class="white-box-content booking-whitebox-content">
				<p class="price-title"><b>RM199.00/</b>month</p>
                <table class="details-table">
                	<tr>
                    	<td>-</td>
                        <td> RM99 only if sign up on Nov 2019</td>
                    </tr>                  
                </table>
            </div>           
        </button> 
    </div>    
    <div class="two-box-container hover-a"  onclick="window.location.href ='adminAddBookingDetails.php';">
        <button class="two-box-div overflow booking-button">
            <div class="color-header darkgreen-header">
                <img src="img/calendar.png" class="header-icon" alt="Daily Pass" title="Daily Pass"> <p>Daily Pass</p>
            </div>
            <div class="white-box-content booking-whitebox-content">
				<p class="price-title"><b>RM30.00/</b>day</p>
                <table class="details-table">
                	<tr>
                    	<td>-</td>
                        <td> RM20 only if sign up on Nov 2019</td>
                    </tr>                 
                </table>
            </div>
        </button>
   </div>     
</div>


<?php include 'js.php'; ?>
</body>
</html>