<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Booking.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $area_type = rewrite($_POST["area_type"]);
    $cost = rewrite($_POST["cost"]);
}


$conn->close();

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Your Location | Cosiety" />
<title>Your Location | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">

    <!-- <h2 class="backend-title-h2">Choose your seat (3)</h2> -->
    <h2 class="backend-title-h2">Your Location</h2>
	<input type="text" class="three-select clean width100" placeholder="Your Location" id="project_title" name="project_title">    
	<div class="gmap-div">
		<iframe class="gmap-iframe" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3971.7238700054518!2d100.31059581426426!3d5.458810736138569!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x304ac3245a61f667%3A0x370934fb6b9a3387!2sStraits%20Quay%20Convention%20Center!5e0!3m2!1sen!2smy!4v1574913730746!5m2!1sen!2smy" frameborder="0" style="border:0;" allowfullscreen=""></iframe>    
    </div>	
    <div class="clear"></div> 
       
    <!-- <div class="fillup-extra-space"></div><a href="paymentMethod.php"><button class="blue-btn payment-button clean">Proceed to Payment</button></a> -->

    <div class="fillup-extra-space"></div>
    <!-- <button input type="submit" name="submit" value="Submit" class="blue-btn payment-button clean">Proceed to Payment</button> -->
    <button input type="submit" name="submit" value="Submit" class="blue-btn payment-button clean"  onclick="window.location.href ='confirmChangeLocation.php';">Next</button>

    <div class="clear"></div>
    <div class="fillup-extra-space2"></div><a  onclick="goBack()" class="cancel-a hover-effect">Cancel</a>
</form>
</div>


<?php include 'js.php'; ?>
</body>
</html>