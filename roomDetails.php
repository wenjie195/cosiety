<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Booking.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/RoomPrice.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$roomDetails = getRoomPrice($conn, "WHERE display = ? ORDER BY type_id ASC", array("display"), array(1), "i");

$conn->close();

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Outstanding Plan | Cosiety" />
<title>Outstanding Plan | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1 hover1 align-select-h1 issue-h1">Room	Details</h1>

    <div class="clear"></div>
    <!-- <h2 class="backend-title-h2 review-title">Total: RM100.00</h2>       -->
    <div class="small-divider"></div>
    <div class="width100">
    	<div class="overflow-scroll-div">
            <table class="issue-table">
            	<tr>
                	<thead>
                    	<th>No.</th>
                        <th>Type</th>
                        <th>Name</th>
                        <th>Price (RM)</th>
                        <th>Duration</th>
                        <th>Discount (%)</th>
                        <th>Description</th>
                        <th>Edit</th>
                    </thead>
                </tr>
                <tbody>

                <?php
                $conn = connDB();
                if($roomDetails)
                {
                    for($cnt = 0;$cnt < count($roomDetails) ;$cnt++)
                    {?>
                        <!-- <tr class="link-to-details"> -->
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $roomDetails[$cnt]->getType  ();?></td>

                            <td><?php echo $roomDetails[$cnt]->getName()?>
                                </td>

                            <td><?php echo $roomDetails[$cnt]->getPrice();?></td>
                            <td><?php echo $roomDetails[$cnt]->getDuration();?></td>
                            <td>
                                <?php echo $roomDetails[$cnt]->getDiscount()?>
                            </td>
                            <td>
                                <?php echo $roomDetails[$cnt]->getDescription()?>
                            </td>
                            <td>
                                <form action="editRoom.php" method="POST">
                                    <button class="clean edit-anc-btn hover1" type="submit" name="roomName" value="<?php echo $roomDetails[$cnt]->getName();?>">
                                        <img src="img/edit-button.png" class="edit-announcement-img hover1a" alt="Edit Room" title="Edit Room">
                                        <img src="img/edit-button.png" class="edit-announcement-img hover1b" alt="Edit Room" title="Edit Room">
                                    </button>
                                </form>
                            </td>

                        </tr>
                        <?php
                    }
                }
                //$conn->close();
                ?>
                </tbody>
            </table><br><div class="three-btn-container">
            <a href="restoreRoom.php" class="add-a"><button name="restore" class="confirm-btn text-center white-text clean black-button anc-ow-btn two-button-side two-button-side1">Restore</button></a>
              <a href="addRoom.php" class="add-a"><button name="add" class="confirm-btn text-center white-text clean black-button anc-ow-btn two-button-side  two-button-side2">Add</button></a>
            </div><br><br>


		</div>
    </div>
</div>


<?php include 'js.php'; ?>
</body>
</html>
