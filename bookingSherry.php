<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];


$conn->close();

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Booking | Cosiety" />
<title>Booking | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1">Booking <a href="addBooking.php" class="hover1"><img src="img/add.png" class="add-icon hover1a"><img src="img/add2.png" class="add-icon hover1b"></a></h1>

    <h4> <?php echo $userDetails->getUsername();?> </h4>

        <!-- Add class booking for booking day inside the div class="day" inside calendar.js-->
        <div class="two-box-div">
             <html ng-app="myApp" ng-controller="AppCtrl" lang="en">
              <head>
                <meta charset="utf-8">
                <title>Circle</title>
              </head>
              <body>
                <div calendar class="calendar" id="calendar"></div>
              </body>
            </html>
           
        </div>

        <div class="two-box-div overflow second-box">
            <div class="color-header orange-header align-calendar-header">
                <img src="img/booking.png" class="header-icon" alt="Booking" title="Booking"> <p>All Booking</p>
                <select class="hover-effect white-text view-a clean year-select">
                	<option>2019</option>
                    <option>2018</option>
                </select>
            </div>
                <select class="hover-effect black-text right-select clean">
                	<option>August Only</option>
                    <option>July Only</option>
                </select>            
            <div class="white-box-content">
            	<a href="receipt.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/seat.png" class="white-icon2 hover-effect" alt="Basic Plan E" title="Basic Plan E"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect">Today    10:00 am - Today 6:00 pm</p>
                            <p class="white-box-content-p hover-effect">Co-Working Space - No.1</p>
                        </div>
                    </div>
                </a>
                <a href="receipt.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/seat.png" class="white-icon2 hover-effect" alt="Basic Plan C" title="Basic Plan C"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect">Tomorrow    10:00 am - Tomorrow    6:00 pm</p>
                            <p class="white-box-content-p hover-effect">Co-Working Space - No.1</p>
                        </div>
                    </div>
                </a>
                <a href="receipt.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/seat.png" class="white-icon2 hover-effect" alt="Basic Plan C" title="Basic Plan C"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect">12/8/2019   10:00 am - 12/8/2019   6:00 pm</p>
                            <p class="white-box-content-p hover-effect">Co-Working Space - No.1</p>
                        </div>
                    </div> 
                </a>                       
            </div>            
        </div> 

        <div class="clear"></div>
        <div class="fillup-leftspace2"></div><a href="addBooking.php"><div class="blue-btn add-new-btn">Add New Booking</div></a>
  		<div class="clear"></div>
        <div class="divider"></div>
     
</div>


<?php include 'js.php'; ?>
</body>
</html>