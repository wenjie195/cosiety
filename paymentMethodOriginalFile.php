<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $area_type = rewrite($_POST["area_type"]);
    $cost = rewrite($_POST["cost"]);
    $duration = rewrite($_POST["duration"]);
    $start_date = rewrite($_POST["start_date"]);
    $total_ppl = rewrite($_POST["total_ppl"]);
    $order_by = rewrite($_POST["order_by"]);
    $discount = rewrite($_POST["discount"]);
    $total_price = rewrite($_POST["total_price"]);
    $total_seat = rewrite($_POST["total_seat"]);
    $project_title = rewrite($_POST["project_title"]);
    $project_details = rewrite($_POST["project_details"]);
}

$conn->close();

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Payment Method | Cosiety" />
<title>Payment Method | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">

<!-- <h4> <?php //echo $area_type?> </h4>
<h4> <?php //echo $cost?> </h4>
<h4> <?php //echo $duration?> </h4>
<h4> <?php //echo $start_date?> </h4>
<h4> <?php //echo $total_ppl?> </h4>
<h4> <?php //echo $order_by?> </h4>
<h4> <?php //echo $discount?> </h4>
<h4> <?php //echo $total_price?> </h4>
<h4> <?php //echo $total_seat?> </h4>
<h4> <?php //echo $project_title?> </h4>
<h4> <?php //echo $project_details?> </h4> -->

	<h1 class="backend-title-h1">Payment</h1>
            <p class="thick-payment-p">Total: RM1378.00</p>    
    <!--
    <div class="half-div-radio">
        <label class="container2">
          <div class="payment1-div">
          	<p class="thin-payment-p">One Time Payment (Discount 10%)</p>
            <p class="thick-payment-p">Total: RM1378.00</p>
          </div>
          <input type="radio" checked="checked" name="radio">
          <span class="checkmark2"></span>
        </label>
    </div>
    <div class="half-div-radio">    
        <label class="container2">
          <div class="payment1-div">
          	<p class="thin-payment-p">Monthly Payment (No Extra Discount)</p>
            <p class="thick-payment-p">Total: RM760.00/month</p>
          </div>       
          <input type="radio" name="radio">
          <span class="checkmark2"></span>
        </label>
    </div>-->
    <h2 class="backend-title-h2">Choose Your Payment Method</h2>  
    <div class="three-div-radio">
        <label class="container2"><img src="img/ipay88.png" class="payment-img" alt="ipay88" title="ipay88">      
          <!-- <input type="radio" name="radio1"> -->
          <input type="radio" id="ipay88" name="ipay88">
          <span class="checkmark2"></span>
        </label>    
    </div>
    <div class="three-div-radio">
        <label class="container2"><img src="img/visa-mastercard.png" class="payment-img" alt="Visa/Master Card" title="Visa/Master Card">      
          <!-- <input type="radio" name="radio1"> -->
          <input type="radio" id="Visa_or_MasterCard" name="Visa_or_MasterCard">
          <span class="checkmark2"></span>
        </label>    
    </div>    
    <div class="three-div-radio no-margin-right">
        <label class="container2">Online Banking      
          <!-- <input type="radio" name="radio1"> -->
          <input type="radio" id="online_banking" name="online_banking">
          <span class="checkmark2"></span>
        </label>    
    </div>      
    <div class="clear"></div>
    <div class="fillup-extra-space"></div><a href="reserveSpace.php"><button class="blue-btn payment-button clean next-btn">Next</button></a>
    <div class="clear"></div>
    <div class="fillup-extra-space2"></div><a  onclick="goBack()" class="cancel-a hover-effect">Cancel</a>
</div>


<?php include 'js.php'; ?>
</body>
</html>