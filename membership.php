<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Membership | Cosiety" />
<title>Membership | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1">My Ongoing Plan</h1>
    <div class="two-box-container">
        <div class="two-box-div overflow">
            <div class="color-header red-header">
                <img src="img/calendar.png" class="header-icon" alt="My Ongoing Plan" title="My Ongoing Plan"> <p>Lounge</p>
                <!--<a href="" class="hover-effect white-text view-a">View All</a>-->
            </div>
            <div class="white-box-content">
            	<a href="receipt.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/seat.png" class="white-icon2 hover-effect" alt="Basic Plan E" title="Basic Plan E"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect">Today    10:00 am - Today 6:00 pm</p>
                            <p class="white-box-content-p hover-effect">Co-Working Space - No.1</p>
                        </div>
                    </div>
                </a>
                <a href="receipt.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/seat.png" class="white-icon2 hover-effect" alt="Basic Plan C" title="Basic Plan C"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect">Tomorrow    10:00 am - Tomorrow    6:00 pm</p>
                            <p class="white-box-content-p hover-effect">Co-Working Space - No.2</p>
                        </div>
                    </div>
                </a>
                <a href="receipt.php" class="hover-effect">               
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/seat.png" class="white-icon2 hover-effect" alt="Basic Plan C" title="Basic Plan C"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect">12/8/2019   10:00 am - 12/8/2019   6:00 pm</p>
                            <p class="white-box-content-p hover-effect">Co-Working Space - No.3</p>
                        </div>
                    </div>   
                </a>                     
            </div>
        </div>
        <div class="two-box-div overflow second-box">
            <div class="color-header orange-header">
                <img src="img/bill.png" class="header-icon" alt="Upcoming Payment" title="Upcoming Payment"> <p>Upcoming Payment</p>
                <!--<a href="booking.php" class="hover-effect white-text view-a">View All</a>-->
            </div>
            <div class="white-box-content">
            	<a href="paymentMethod.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/meeting-room.png" class="white-icon2 hover-effect" alt="Lounge" title="Lounge"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect left-date">Expire on 12/9/2019    10:00 am</p><p class="black-text right-price">RM99.00</p>
                            <p class="white-box-content-p hover-effect clear">Lounge - Monthly Membership</p>
                        </div>
                    </div>
                </a>
                <a href="paymentMethod.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/group.png" class="white-icon2 hover-effect" alt="Private Suit 1 Work Station" title="Private Suit 1 Work Station"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect left-date">Expire on 13/8/2019   10:00 am</p><p class="black-text right-price">RM1000.00</p>
                            <p class="white-box-content-p hover-effect clear">Private Suit 1 Work Station</p>
                        </div>
                    </div>
                </a>
                <a href="paymentMethod.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/group.png" class="white-icon2 hover-effect" alt="Private Suit 2 Work Stations" title="Private Suit 2 Work Stations"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect left-date">Expire on 14/8/2019   10:00 am</p><p class="black-text right-price">RM1600.00</p>
                            <p class="white-box-content-p hover-effect clear">Private Suit 2 Work Stations</p>
                        </div>
                    </div> 
                </a>                       
            </div>            
        </div> 
    </div>  

        <!-- Add class booking for booking day inside the div class="day" inside calendar.js-->
        <div class="two-box-div">
            <div class="color-header blue-header top-radius">
                <img src="img/expired-plan.png" class="header-icon" alt="Expired Plan" title="Expired Plan"> <p>Expired Plan</p>
                <a href="allPlan.php" class="hover-effect white-text view-a">View All</a>
            </div>
            <div class="white-box-content">
            	<a href="receipt.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/meeting-room.png" class="white-icon2 hover-effect" alt="Basic Plan E" title="Basic Plan E"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect left-date">Expire on 12/8/2019    10:00 am <span class="green-status">(PAID)</span></p><p class="black-text right-price">RM99.00</p>
                            <p class="white-box-content-p hover-effect clear">Lounge - Monthly Membership</p>
                        </div>
                    </div>
                </a>
                <a href="receipt.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/meeting-room.png" class="white-icon2 hover-effect" alt="Basic Plan E" title="Basic Plan E"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect left-date">Expire on 13/7/2019   10:00 am <span class="green-status">(PAID)</span></p><p class="black-text right-price">RM99.00</p>
                            <p class="white-box-content-p hover-effect clear">Lounge - Monthly Membership</p>
                        </div>
                    </div>
                </a>
                <a href="receipt.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/meeting-room.png" class="white-icon2 hover-effect" alt="Basic Plan E" title="Basic Plan E"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect left-date">Expire on 14/6/2019   10:00 am <span class="green-status">(PAID)</span></p><p class="black-text right-price">RM99.00</p>
                            <p class="white-box-content-p hover-effect clear">Lounge - Monthly Membership</p>
                        </div>
                    </div> 
                </a>                       
            </div>  
        </div>
        <div class="two-box-div overflow second-box image-container">  
            <!-- Crop image into 16:9, preset the booking details to the plan and discount details. -->
            <a href="addBookingDetails.php" class="hover-effect"><img src="img/promotion.jpg" class="width100" alt="Promotion" title="Promotion"></a>
        </div>  
  
     
</div>


<?php include 'js.php'; ?>
</body>
</html>