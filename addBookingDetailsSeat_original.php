<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Booking Details (Seat) | Cosiety" />
<title>Booking Details (Seat) | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
<form action="paymentMethod.php" method="POST">

    <!-- <h1 class="backend-title-h1">Co-Working Space (Hot Seat)</h1> -->
    <h1 class="backend-title-h1">Dedicated Work Desk - Straits Quay</h1>
    <!-- <h1 class="backend-title-h1"><?php //echo $area_type?></h1> -->

    <div class="clear"></div>
	<h2 class="backend-title-h2">Floor Plan</h2>    
    <img src="img/zone2.jpg" alt="Floor Plan" title="Floor Plan" class="floorplan-img">
    <div class="clear"></div>
    <div class="four-img-container">
        <div class="four-img-div">
            <a href="./img/working-space1.jpg"  data-fancybox="images-preview" title="Co-Working Space (Hot Seat)">
                <img src="img/working-space1.jpg" class="width100 opacity-hover" alt="Co-Working Space (Hot Seat)" title="Co-Working Space (Hot Seat)">
            </a>  
            <p class="four-img-p">Co-Working Space</p>  	
        </div>
        <div class="four-img-div middle-four-img-div2">
            <a href="./img/working-space3.jpg"  data-fancybox="images-preview" title="Co-Working Space">
                <img src="img/working-space3.jpg" class="width100 opacity-hover" alt="Co-Working Space" title="Co-Working Space">
            </a>  
            <p class="four-img-p">Co-Working Space</p>  	
        </div>                          
	</div>
	<h2 class="backend-title-h2">Choose your seat</h2>    
    <div class="big-container-for-seat">
    	<div class="eight-checkbox">
            <label class="container1"> 1
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>
    	<div class="eight-checkbox">
            <label class="container1"> 2
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 3
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 4
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>
    	<div class="eight-checkbox">
            <label class="container1"> 5
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 6
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>
    	<div class="eight-checkbox">
            <label class="container1"> 7
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 8
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 9
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div> 
    	<div class="eight-checkbox">
            <label class="container1"> 10
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 11
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 12
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 13
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div> 
    	<div class="eight-checkbox">
            <label class="container1"> 14
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 15
              <input type="checkbox" disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 16
              <input type="checkbox" disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div> 
    	<div class="eight-checkbox">
            <label class="container1"> 17
              <input type="checkbox" disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 18
              <input type="checkbox" disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>   
    	<div class="eight-checkbox">
            <label class="container1"> 19
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>
    	<div class="eight-checkbox">
            <label class="container1"> 20
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 21
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>
    	<div class="eight-checkbox">
            <label class="container1"> 22
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 23
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 24
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div> 
    	<div class="eight-checkbox">
            <label class="container1"> 25
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 26
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 27
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 28
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div> 
    	<div class="eight-checkbox">
            <label class="container1"> 29
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 30
              <input type="checkbox" disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 31
              <input type="checkbox" disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div> 
    	<div class="eight-checkbox">
            <label class="container1"> 32
              <input type="checkbox" disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 33
              <input type="checkbox" disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>         
    	<div class="eight-checkbox">
            <label class="container1"> 34
              <input type="checkbox" disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>           
                                                                                      
    </div>
    <div class="clear"></div>

	<p class="grey-text input-top-p">Project Title</p>
	<input type="text" class="three-select clean width100" placeholder="Key in Project Title" id="project_title" name="project_title">    
 	<p class="grey-text input-top-p project-p">Project Details</p>
    <textarea class="clean width100 project-textarea" placeholder="Key in Project Details" id="project_details" name="project_details"></textarea>  
       
    <!-- <div class="fillup-extra-space"></div><a href="paymentMethod.php"><button class="blue-btn payment-button clean">Proceed to Payment</button></a> -->

    <div class="fillup-extra-space"></div>
    <!-- <button input type="submit" name="submit" value="Submit" class="blue-btn payment-button clean">Proceed to Payment</button> -->
    <button input type="submit" name="submit" value="Submit" class="blue-btn payment-button clean">Submit</button>

    <div class="clear"></div>
    <div class="fillup-extra-space2"></div><a  onclick="goBack()" class="cancel-a hover-effect">Cancel</a>
</form>
</div>


<?php include 'js.php'; ?>
</body>
</html>