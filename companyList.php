<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Company List | Cosiety" />
<title>Company List | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1 align-select-h1">Company  <a href="addMember.php" class="hover1"><img src="img/add.png" class="add-icon hover1a" alt="Add a New Company" title="Add a New Company"><img src="img/add2.png" class="add-icon hover1b" alt="Add a New Company" title="Add a New Company"></a>
     | <a class="lightblue-text hover-effect" href="member.php">Member</a> <!--<a href="addMember.php"><img src="img/add.png" class="add-icon hover1a" alt="Add Member" title="Add Member"><img src="img/add2.png" class="add-icon hover1b" alt="Add Member" title="Add Member"></a>-->
    </h1>
	<select class="clean align-h1-select">
    	<option>Latest</option>
        <option>Oldest</option>
    </select>
	<div class="clear"></div>
    <div class="width100 search-div overflow">
    	<div class="three-search-div">
        	<p class="upper-search-p">Company</p>
            <input class="search-input" type="text" placeholder="Company Name">
        </div>
    	<div class="three-search-div middle-three-search second-three-search">
        	<p class="upper-search-p">Size</p>
            <select class="search-input">
            	<option>< 10 </option>
                <option>10-50</option>
                <option>50 - 100</option>
                <option>> 100</option>         
            </select>
        </div>
        <div class="three-search-div">
        	<p class="upper-search-p">Employer</p>
            <input class="search-input" type="text" placeholder="Employer Name">
        </div>
    	<div class="three-search-div second-three-search">
        	<p class="upper-search-p">Start Date</p>
            <input class="search-input" type="date" >
        </div>        
    	<div class="three-search-div middle-three-search">
        	<p class="upper-search-p">End Date</p>
            <input class="search-input" type="date" >
        </div>
    	<div class="three-search-div second-three-search">
			<button class="three-search blue-btn clean search-blue-btn">Search</button>
        </div>
            
    </div>
    <div class="clear"></div>    
    <div class="small-divider"></div>
    <div class="width100">
    	<div class="overflow-scroll-div">    
            <table class="issue-table">
            	<tr>
                	<thead>
                    	<th>No.</th>
                        <th>Company</th>
                        <th>Staff</th>
                        <th>Employer</th>
                        <th>Created On</th>
                        <th>Status</th>                      
                    </thead>
                </tr>
                <tr data-url="companyDetails.php" class="link-to-details hover-effect">
                	<td>1.</td>
                    <td>XXX Company</td>
                    <td>5</td>
                    <td>Janice Lim</td>      
                    <td>12/8/2019</td>
                    <td class="green-status">Active</td>               
                </tr>
                <tr data-url="companyDetails.php" class="link-to-details hover-effect">
                	<td>2.</td>
                    <td>ABC Company</td>
                    <td>5</td>                                        
                    <td>Alicia Lim</td>       
                    <td>12/8/2019</td>
                    <td class="red-text">Blacklisted</td>    
                </tr>                
            </table>
		</div>
    </div>
  		<!--
        <div class="clear"></div>
        <div class="fillup-leftspace"></div><a href="addBooking.php"><div class="blue-btn add-new-btn">Add New Booking</div></a>-->
  
     
</div>


<?php include 'js.php'; ?>
</body>
</html>