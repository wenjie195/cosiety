<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Booking.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/RoomPrice.php';
require_once dirname(__FILE__) . '/classes/BookingPrivate.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$date = date("Y-m-d");

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
  $id = rewrite($_POST["roomId"]);
}


//$conn->close();
$roomDetails = getRoomPrice($conn, "WHERE id = ?", array("id"), array($id), "i");
$privateSuitDetails = getPrivate($conn, "WHERE seat_status =?", array("seat_status"), array(0), "i");

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Booking Details | Cosiety" />
<title>Booking Details | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">

    <!-- <form action="addBookingDetailsSeatPrivate.php" method="POST"> -->
    <form action="paymentMethodPrivate.php" method="POST">

        <h1 class="backend-title-h1"><?php echo $roomDetails[0]->getName(); ?></h1>
        <input type="hidden" name="title" value="<?php echo $roomDetails[0]->getName(); ?>">
        <!-- <input type="hidden" id="area_type" name="area_type" value="Dedicated Work Desk - Straits Quay"> -->

        <!-- <div class="three-div">
            <p class="grey-text input-top-p">Duration (Month)</p>
            <input class="three-select clean" id="duration" name="duration" type="number" value="duration">
        </div> -->

        <div class="three-div">
            <p class="grey-text input-top-p">Duration (<?php echo $roomDetails[0]->getDuration(); ?>)</p>
            <input type="hidden" name="timeline" value="<?php echo $roomDetails[0]->getDuration(); ?>">
            <select class="three-select clean" id="duration" name="duration">
                <option value="1" name="1">1</option>
                <option value="2" name="2">2</option>
                <option value="3" name="2">3</option>
                <option value="4" name="4">4</option>
                <option value="5" name="5">5</option>
                <option value="6" name="6">6</option>
                <option value="7" name="7">7</option>
                <option value="8" name="8">8</option>
                <option value="9" name="9">9</option>
                <option value="10" name="10">10</option>
                <option value="11" name="11">11</option>
                <option value="12" name="12">12</option>
            </select>
        </div>

        <div class="three-div middle-three-div second-three-div">
            <p class="grey-text input-top-p">Total Seat (
              <?php
                if ($privateSuitDetails) 
                  {
                    echo count($privateSuitDetails); ?> Seat Available)</p>
                  <?php 
                  }
                else
                  {
                    echo 0; ?> Seat Available)</p>
                  <?php
                  }
              ?>
            
            <p class="three-select-p"><?php echo $roomDetails[0]->getRoomCapacity(); ?></p>

            <input type="hidden" name="total_people" value="<?php echo $roomDetails[0]->getRoomCapacity(); ?>">

            <!-- <select class="three-select clean" id="total_people" name="total_people"> -->
              <!-- <?php
              //$add = 1;
              //for ($cnt=0; $cnt <count($privateSuitDetails) ; $cnt++) 
              //{
              //  $new = $add++;
                ?>
                <option value="<?php echo $new ?>" name="total_people"><?php echo $new ?></option>
                <?php
              //}
              //if (!$privateSuitDetails) 
              //{
                ?>
                  <option value="0" name="total_people">0</option>
                <?php
              //}
              ?> -->

            <!-- </select> -->
        </div>

        <div class="three-div">
            <p class="grey-text input-top-p">Start Date</p>
            <input class="three-select clean" type="date" id="start_date" name="start_date" value="<?php echo $date ?>">
            <!-- <input type="date" class="three-select clean"> -->
        </div>

        <!-- <div class="tempo-three-clear"></div> -->

        <!-- <div class="three-div">
            <p class="grey-text input-top-p">Discount</p>
            <p class="three-select-p"><?php //echo $roomDetails[0]->getDiscount(); ?>%</p>
            <input type="hidden" name="discount" id="discount" value="<?php //echo $roomDetails[0]->getDiscount(); ?>">
        </div> -->

        <div class="tempo-three-clear"></div>

        <!-- <div class="three-div">
            <p class="grey-text input-top-p">Price</p>
            <p class="total-p"><b>RM799.00/</b>month</p>
            <input type="hidden" name="total_price" id="total_price" value="RM639.20">
        </div> -->

        <!-- <div class="three-div">
            <p class="grey-text input-top-p">Discount</p>
            <p class="total-p"><b>20%<b></p>
            <input type="hidden" name="discount" id="discount" value="20%">
        </div> -->

        <div class="three-div second-three-div">
            <!-- <p class="grey-text input-top-p">Total</p> -->
            <p class="grey-text input-top-p">Price</p>
            <p class="total-p">RM<?php echo $roomDetails[0]->getPrice() ?>/Month</p>
            <input type="hidden" name="total_price" id="total_price" value="<?php echo $roomDetails[0]->getPrice() ?>">
        </div>

        <div class="three-div">
            <p class="grey-text input-top-p">Discount</p>
            <p class="three-select-p"><?php echo $roomDetails[0]->getDiscount(); ?>%</p>
            <input type="hidden" name="discount" id="discount" value="<?php echo $roomDetails[0]->getDiscount(); ?>">
        </div>

        <div class="clear"></div>
        <div class="divider"></div>

        <!-- <div class="fillup-extra-space"></div><a href="paymentMethod.php"><button class="blue-btn payment-button clean">Proceed to Payment</button></a> -->

        <div class="fillup-extra-space"></div>
        <?php
        if ($privateSuitDetails) {
          ?><button input type="submit" name="submit" value="Submit" class="blue-btn payment-button clean" >Next</button><?php
        }
         ?>
        <!-- <button input type="submit" name="submit" value="Submit" class="blue-btn payment-button clean">Proceed to Payment</button> -->


        <div class="clear"></div>

    </form>
</div>


<?php include 'js.php'; ?>
</body>
</html>
