<?php


require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/rate.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';



$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    // echo $_POST['update_icno'].'<br>';
    $uid = $_SESSION['uid'];

    $referralBonus = rewrite($_POST["referral_bonus"]);
    $commission = rewrite($_POST["commission"]);
    $conversionPoint = rewrite($_POST["conversion_point"]);
    $chargesWithdraw = rewrite($_POST["charges_withdraw"]);
    $pointVoucher = rewrite($_POST["point_voucher"]);
}

$products = getProduct($conn);

$rateRows = getRate($conn," WHERE id = ? ",array("id"),array(1),"i");
$rateDetails = $rateRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/adminRate.php" />
    <meta property="og:title" content="Adjust the Rate | DCK Supreme" />
    <title>Adjust the Rate | DCK Supreme</title>
    <meta property="og:description" content="DCK® Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK® Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK®, dck supreme,supreme,dck, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration,
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/adminRate.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding product-det admin-big-div rate-div">
	<h1 class="details-h1">
        Adjust the Rate
    </h1>

<form method="POST" onsubmit="return editprofileFunc(name);" action="utilities/rateFunction.php">
    <div class="left50">
    	<p>Referral Bonus (RM)</p>
      <input class="clean product-input" type="number" id="referral_bonus" name="referral_bonus" placeholder="" value="<?php echo $rateDetails->getReferralBonus(); ?>">
    </div>
    <div class="left50 right50">
    	<p>Commission per Product (%)</p>
    	<input class="clean product-input" type="number" id="commission" name="commission" placeholder="" value="<?php echo $rateDetails->getCommission(); ?>">
    </div>

    <div class="left50">
    	<p>% per Sale Convert to Points</p>
    	<input class="clean product-input" type="number" placeholder="70" >
    </div>
    <div class="left50 right50">
    	<p>Conversion of 1 Point to RM (RM)</p>
    	<input class="clean product-input" type="number" id="conversion_point" name="conversion_point" placeholder="0.01" value="<?php echo $rateDetails->getConversionPoint(); ?>">
    </div>

    <div class="left50">
    	<p>Total Charges For any Succesful Withdraw</p>
    	<input class="clean product-input" type="number" id="charges_withdraw" name="charges_withdraw" value="<?php echo $rateDetails->getChargesWithdraw(); ?>">
    </div>
    <div class="left50 right50">
    	<p>Total Points to Gain 1 Voucher</p>
    	<input class="clean product-input" type="number" id="point_voucher" name="point_voucher" placeholder="100" value="<?php echo $rateDetails->getPointVoucher(); ?>">
    </div>

    <div class="clear"></div>

    <div class="three-btn-container">
    <button class="shipout-btn-a black-button three-btn-a" type="submit" id = "admin_rate" name = "admin_rate" ><b>CONFIRM</b></a></button>
    </div>
</div>

</div>
</form>
<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Rate Update Success!";
        }
        if($_GET['type'] == 12)
        {
            $messageType = "Withdraw Error!";
        }
        if($_GET['type'] == 12)
        {
            $messageType = "Picture upload failed, please try again.";
        }
        if($_GET['type'] == 13)
        {
            $messageType = "Please select a picture.";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 2)
        {
            $messageType = "The Amount Should Atleast RM1 Left On The Wallet!!";
        }
        if($_GET['type'] == 12)
        {
            $messageType = "Withdraw Error!";
        }
        if($_GET['type'] == 12)
        {
            $messageType = "Picture upload failed, please try again.";
        }
        if($_GET['type'] == 13)
        {
            $messageType = "Please select a picture.";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
    if($_SESSION['messageType'] == 3)
    {
        if($_GET['type'] == 3)
        {
            $messageType = "Wrong E-Pin!";
        }
        if($_GET['type'] == 12)
        {
            $messageType = "Withdraw Error!";
        }
        if($_GET['type'] == 12)
        {
            $messageType = "Picture upload failed, please try again.";
        }
        if($_GET['type'] == 13)
        {
            $messageType = "Please select a picture.";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
  }
    ?>
<!-- <script>
$(function () {
    $('.link-to-details').click(function () {
        window.location.href = $(this).data('url');
    });
})

</script>
<script>
function goBack() {
  window.history.back();
}
</script> -->




</body>
</html>
