<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BookingWorkDesk.php';
require_once dirname(__FILE__) . '/classes/BookingPrivate.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

// $bookingDetails = getWork($conn);
$bookingDetails = getWorkDesk($conn," WHERE seat_status = ? ",array("seat_status"),array(0),"i");

  if($_SERVER['REQUEST_METHOD'] == 'POST')
  {
      $area_type = rewrite($_POST["area_type"]);
      $duration = rewrite($_POST["duration"]);
      $start_date = rewrite($_POST["start_date"]);
      $discount = rewrite($_POST["discount"]);
      $total_price = rewrite($_POST["total_price"]);
      $title = rewrite($_POST["title"]);
      $totalPeople = rewrite($_POST["total_people"]);
      $timeLine = rewrite($_POST["timeline"]);
  }


$conn->close();

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Booking Details (Seat) | Cosiety" />
<title>Booking Details (Seat) | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">

  <form action="paymentMethodPrivate.php" method="POST">

  <!-- <h1 class="backend-title-h1">Co-Working Space (Hot Seat)</h1> -->
  <h1 class="backend-title-h1"><?php echo $title ?> - Straits Quay</h1>
  <input type="hidden" name="title" value="<?php echo $title ?>">
  <!-- <h1 class="backend-title-h1"><?php //echo $area_type?></h1> -->

  <input type="hidden" name="area_type" id="area_type" value="<?php echo $area_type?>">
  <input type="hidden" name="duration" id="duration" value="<?php echo $duration?>">
  <input type="hidden" name="start_date" id="start_date" value="<?php echo $start_date?>">
  <input type="hidden" name="discount" id="discount" value="<?php echo $discount?>">
  <input type="hidden" name="total_price" id="total_price" value="<?php echo $total_price?>">
  <input type="hidden" name="total_people" id="total_people" value="<?php echo $totalPeople?>">
  <input type="hidden" name="timeline" value="<?php echo $timeLine; ?>">

  <div class="clear"></div>

  <h2 class="backend-title-h2">Floor Plan</h2>
    <img src="img/zone3.jpg" alt="Floor Plan" title="Floor Plan" class="floorplan-img">
    <div class="clear"></div>
    <div class="four-img-container">
        <div class="four-img-div">
            <a href="./img/working-space2.jpg"  data-fancybox="images-preview" title="Private Suit">
                <img src="img/working-space2.jpg" class="width100 opacity-hover" alt="Private Suit" title="Private Suit">
            </a>
            <p class="four-img-p">Private Suit</p>
        </div>
        <div class="four-img-div middle-four-img-div2">
            <a href="./img/working-space4.jpg"  data-fancybox="images-preview" title="Private Suit">
                <img src="img/working-space4.jpg" class="width100 opacity-hover" alt="Private Suit" title="Private Suit">
            </a>
            <p class="four-img-p">Private Suit</p>
        </div>
	</div>

   <!-- <p class="grey-text input-top-p">Project Title</p>
   <input type="text" class="three-select clean width100" placeholder="Key in Project Title" id="project_title" name="project_title">

   <p class="grey-text input-top-p project-p">Project Details</p>
   <textarea class="clean width100 project-textarea" placeholder="Key in Project Details" id="project_details" name="project_details"></textarea> -->

   <div class="fillup-extra-space"></div>
   <button input type="submit" name="submit" value="Submit" class="blue-btn payment-button clean">Submit</button>

   <div class="clear"></div>
   <div class="fillup-extra-space2"></div><a  onclick="goBack()" class="cancel-a hover-effect">Cancel</a>


  </form>

</div>

<?php include 'js.php'; ?>
</body>
</html>
