<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Ongoing Plan | Cosiety" />
<title>Ongoing Plan | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1">Ongoing Plan</h1>
    <div class="two-box-container">
        <div class="two-box-div overflow">
            <div class="color-header red-header">
                <img src="img/calendar.png" class="header-icon" alt="Lounge" title="Lounge"> <p>Lounge</p>
                <!--<a href="" class="hover-effect white-text view-a">View All</a>-->
            </div>
            <div class="white-box-content">
            	<a href="receipt.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/calendar.png" class="white-icon2 hover-effect" alt="Lounge Daily Pass" title="Lounge Daily Pass"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect left-date">Today    10:00 am - Today 6:00 pm</p><p class="black-text right-price">RM30.00</p>
                            <div class="clear"></div>
                            <p class="white-box-content-p hover-effect">Lounge Daily Pass</p>
                        </div>
                    </div>
                </a>
                <a href="receipt.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/calendar.png" class="white-icon2 hover-effect" alt="Basic Plan C" title="Basic Plan C"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect left-date">Tomorrow    10:00 am - Tomorrow    6:00 pm</p><p class="black-text right-price">RM30.00</p>
                            <div class="clear"></div>
                            <p class="white-box-content-p hover-effect">Lounge Daily Pass</p>
                        </div>
                    </div>
                </a>
                <a href="receipt.php" class="hover-effect">               
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/calendar.png" class="white-icon2 hover-effect" alt="Basic Plan C" title="Basic Plan C"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect left-date">12/8/2019   10:00 am - 12/8/2019   6:00 pm</p><p class="black-text right-price">RM30.00</p>
                            <div class="clear"></div>
                            <p class="white-box-content-p hover-effect">Lounge Daily Pass</p>
                        </div>
                    </div>   
                </a>                     
            </div>
        </div>
        <div class="two-box-div overflow second-box">
            <div class="color-header orange-header">
                <img src="img/seat2.png" class="header-icon" alt="Work Desk" title="Work Desk"> <p>Work Desk</p>
                <!--<a href="booking.php" class="hover-effect white-text view-a">View All</a>-->
            </div>
            <div class="white-box-content">
            	<a href="receipt.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/seat2.png" class="white-icon2 hover-effect" alt="Dedicated Work Desk" title="Dedicated Work Desk"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect left-date">1/12/2019   10:00 am - 31/12/2019   6:00 pm</p><p class="black-text right-price">RM799.00</p>
                            <p class="white-box-content-p hover-effect clear">Dedicated Work Desk No.10</p>
                        </div>
                    </div>
                </a>
                <a href="receipt.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/seat2.png" class="white-icon2 hover-effect" alt="Dedicated Work Desk" title="Dedicated Work Desk"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect left-date">1/1/2020   10:00 am - 31/1/2020   6:00 pm</p><p class="black-text right-price">RM799.00</p>
                            <p class="white-box-content-p hover-effect clear">Dedicated Work Desk No.10</p>
                        </div>
                    </div>
                </a>
                <a href="receipt.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/seat2.png" class="white-icon2 hover-effect" alt="Dedicated Work Desk" title="Dedicated Work Desk"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect left-date">1/2/2020   10:00 am - 29/2/2020   6:00 pm</p><p class="black-text right-price">RM799.00</p>
                            <p class="white-box-content-p hover-effect clear">Dedicated Work Desk No.10</p>
                        </div>
                    </div> 
                </a>                       
            </div>            
        </div> 
    </div>  

        <!-- Add class booking for booking day inside the div class="day" inside calendar.js-->
        <div class="two-box-div">
            <div class="color-header blue-header top-radius">
                <img src="img/group.png" class="header-icon" alt="Private Suit" title="Private Suit"> <p>Private Suit</p> 
            </div>
            <div class="white-box-content">
            	<a href="receipt.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/group.png" class="white-icon2 hover-effect" alt="Private Suit" title="Private Suit"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect left-date">1/2/2020   10:00 am - 29/2/2020   6:00 pm</p><p class="black-text right-price">RM1000.00</p>
                            <p class="white-box-content-p hover-effect clear">1 Work Station</p>
                        </div>
                    </div>
                </a>
                <a href="receipt.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/group.png" class="white-icon2 hover-effect" alt="Private Suit" title="Private Suit"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect left-date">1/3/2020   10:00 am - 31/3/2020   6:00 pm</p><p class="black-text right-price">RM1000.00</p>
                            <p class="white-box-content-p hover-effect clear">1 Work Station</p>
                        </div>
                    </div>
                </a>
                <a href="receipt.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/group.png" class="white-icon2 hover-effect" alt="Private Suit" title="Private Suit"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect left-date">1/4/2020   10:00 am - 30/4/2020   6:00 pm</p><p class="black-text right-price">RM1000.00</p>
                            <p class="white-box-content-p hover-effect clear">1 Work Station</p>
                        </div>
                    </div> 
                </a>                       
            </div>  
        </div>
        <div class="two-box-div overflow second-box">
            <div class="color-header purple-header">
                <img src="img/seat2.png" class="header-icon" alt="Meeting Room" title="Meeting Room"> <p>Meeting Room</p>
                <!--<a href="booking.php" class="hover-effect white-text view-a">View All</a>-->
            </div>
            <div class="white-box-content">
            	<a href="receipt.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/meeting-room3.png" class="white-icon2 hover-effect" alt="Dedicated Work Desk" title="Dedicated Work Desk"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect left-date">1/12/2019   10:00 am - 1/12/2019   6:00 pm</p><p class="black-text right-price">RMXX.00</p>
                            <p class="white-box-content-p hover-effect clear">Meeting Room</p>
                        </div>
                    </div>
                </a>
                <a href="receipt.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/meeting-room3.png" class="white-icon2 hover-effect" alt="Dedicated Work Desk" title="Dedicated Work Desk"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect left-date">1/1/2020   10:00 am - 1/1/2020   6:00 pm</p><p class="black-text right-price">RMXX.00</p>
                            <p class="white-box-content-p hover-effect clear">Meeting Room</p>
                        </div>
                    </div>
                </a>
                <a href="receipt.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/meeting-room3.png" class="white-icon2 hover-effect" alt="Dedicated Work Desk" title="Dedicated Work Desk"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect left-date">1/2/2020   10:00 am - 1/2/2020   6:00 pm</p><p class="black-text right-price">RMXX.00</p>
                            <p class="white-box-content-p hover-effect clear">Meeting Room</p>
                        </div>
                    </div> 
                </a>                       
            </div>            
        </div>         
        
        
        
        <div class="two-box-div overflow image-container">  
            <!-- Crop image into 16:9, preset the booking details to the plan and discount details. -->
            <a href="addBookingDetails.php" class="hover-effect"><img src="img/promotion.jpg" class="width100" alt="Promotion" title="Promotion"></a>
        </div>  
  
     
</div>


<?php include 'js.php'; ?>
</body>
</html>