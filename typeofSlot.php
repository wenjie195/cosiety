<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Type of Slot | Cosiety" />
<title>Type of Slot | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1">Type of Slot <a href="addSlot.php" class="hover1"><img src="img/add.png" class="add-icon hover1a" alt="Add Slot" title="Add Slot"><img src="img/add2.png" class="add-icon hover1b" alt="Add Slot" title="Add Slot"></a></h1>
	<div class="clear"></div>
	<a href="editSlot.php">
        <div class="four-white-div">
            <div class="color-circle color-circle1">
                <img src="img/seat3.png" class="circle-icon" alt="Hot Seat"  title="Hot Seat">
            </div>
            <p class="black-text white-div-title">Hot Seat</p>
            
        </div>
    </a>
	<a href="editSlot.php">
        <div class="four-white-div second-white-div third-two four-two">
            <div class="color-circle color-circle2">
                <img src="img/small-meeting-room.png" class="circle-icon" alt="Private Suit "  title="Private Suit ">
            </div>
            <p class="black-text white-div-title">Private Suit </p>
            
        </div>
    </a>   
	<a href="editSlot.php">
        <div class="four-white-div third-white-div">
            <div class="color-circle color-circle3">
                <img src="img/big-meeting-room.png" class="circle-icon" alt="Lounge"  title="Lounge">
            </div>
            <p class="black-text white-div-title">Lounge</p>
            
        </div>
    </a> 
	<a href="editSlot.php">
        <div class="four-white-div four-two">
            <div class="color-circle color-circle4">
                <img src="img/seat2.png" class="circle-icon" alt="Dedicated Work Desk"  title="Dedicated Work Desk">
            </div>
            <p class="black-text white-div-title">Dedicated Work Desk</p>
        </div>
    </a>  
  
     
</div>


<?php include 'js.php'; ?>
</body>
</html>