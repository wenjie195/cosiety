<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Booking.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$uid = $_SESSION['uid'];

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$adminList = getBooking($conn," WHERE uid = ? ",array("uid"),array($uid),"s");

$bookingDetails = getBooking($conn, " WHERE uid = ? ", array( "uid" ), array( $uid ), "s");

$currentDate = date("Y-m-d");

$conn->close();

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Booking | Cosiety" />
<title>Booking | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1 booking-h1">Booking<!--<a href="addBooking.php" class="hover1"><img src="img/add.png" class="add-icon hover1a"><img src="img/add2.png" class="add-icon hover1b"></a>--></h1>
	<a href="addBooking.php"><div class="blue-btn add-new-btn booking-btn">Add New Booking</div></a>
    <div class="clear"></div>
        <!-- Add class booking for booking day inside the div class="day" inside calendar.js-->
        <div class="two-box-div">
            <div class="color-header red-header align-calendar-header top-radius">
                <img src="img/booking.png" class="header-icon" alt="Booking" title="Booking"> <p>Ongoing Booking</p>
                <!-- <select class="hover-effect white-text view-a clean year-select">
                	<option>2019</option>
                    <option>2018</option>
                </select> -->
            </div>
            <div class="white-box-content">
<<<<<<< Updated upstream

								<?php

								if ($bookingDetails) {

									for ($cnt=0; $cnt < count($bookingDetails) ; $cnt++) {

										$endDate = $bookingDetails[$cnt]->getEndDate();
										$duration = $bookingDetails[$cnt]->getDuration();
										$space = " ";
										$timeLine = $bookingDetails[$cnt]->getTimeline();
										$time = $duration . $space . $timeLine;
										$endDate = date("Y-m-d", strtotime(date("Y-m-d", strtotime($endDate)). "+ $time"));

										if ($currentDate < $endDate) {

										?><a href="receipt.php" class="hover-effect">
			                    <div class="content-container">
			                        <div class="left-icon-div green-icon hover-effect"><img src="img/seat.png" class="white-icon2 hover-effect" alt="Basic Plan E" title="Basic Plan E"></div>
			                        <div class="right-icon-div">
			                            <p class="light-grey-text small-date hover-effect"><?php echo date("d-m-Y", strtotime($bookingDetails[$cnt]->getStartDate())) ?> - <?php echo date("d-m-Y", strtotime($bookingDetails[$cnt]->getEndDate())) ?></p>
			                            <p class="white-box-content-p hover-effect"><?php echo $bookingDetails[$cnt]->getAreaType() ?></p>
			                        </div>
			                    </div>
			                </a><?php
										}
									}
								}

								 ?>
            </div>

=======
                <div class="content-container">
                    <!-- <div class="left-icon-div green-icon hover-effect"><img src="img/seat.png" class="white-icon2 hover-effect" alt="Basic Plan E" title="Basic Plan E"></div> -->
                    <div class="right-icon-div">
                            <!-- <p class="light-grey-text small-date hover-effect"><?php //echo $userBookingDetails->getBookingId();?></p>
                            <p class="white-box-content-p hover-effect"><?php //echo $userBookingDetails->getAreaType();?></p>
                            <p class="white-box-content-p hover-effect"><?php //echo $userBookingDetails->getStartDate();?></p> -->
                    </div>
                </div>
            	<!-- <a href="receipt.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/seat.png" class="white-icon2 hover-effect" alt="Basic Plan E" title="Basic Plan E"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect">Today    10:00 am - Today 6:00 pm</p>
                            <p class="white-box-content-p hover-effect">Co-Working Space - No.1</p>
                        </div>
                    </div>
                </a>
                <a href="receipt.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/seat.png" class="white-icon2 hover-effect" alt="Basic Plan C" title="Basic Plan C"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect">Tomorrow    10:00 am - Tomorrow    6:00 pm</p>
                            <p class="white-box-content-p hover-effect">Co-Working Space - No.1</p>
                        </div>
                    </div>
                </a>
                <a href="receipt.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/seat.png" class="white-icon2 hover-effect" alt="Basic Plan C" title="Basic Plan C"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect">12/8/2019   10:00 am - 12/8/2019   6:00 pm</p>
                            <p class="white-box-content-p hover-effect">Co-Working Space - No.1</p>
                        </div>
                    </div> 
                </a>                        -->
            </div>            
           
>>>>>>> Stashed changes
        </div>

        <div class="two-box-div overflow second-box">
            <div class="color-header orange-header align-calendar-header">
                <img src="img/bookmark.png" class="header-icon" alt="Booking" title="Booking"> <p>Booking History</p>
                <!-- <select class="hover-effect white-text view-a clean year-select">
                	<option>2019</option>
                    <option>2018</option>
                </select> -->
            </div>
                <!-- <select class="hover-effect black-text right-select clean">
                	<option>August Only</option>
                    <option>July Only</option>
                </select>             -->
            <div class="white-box-content">
                <!-- <div class="content-container">
                    <div class="left-icon-div green-icon hover-effect"><img src="img/seat.png" class="white-icon2 hover-effect" alt="Basic Plan E" title="Basic Plan E"></div>
                    <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect"><?php //echo $userBookingDetails->getBookingId();?></p>
                            <p class="white-box-content-p hover-effect"><?php //echo $userBookingDetails->getAreaType();?></p>
                            <p class="white-box-content-p hover-effect"><?php  //echo $userBookingDetails->getStartDate();?> | <?php //echo $userBookingDetails->getDuration();?></p>
                    </div>
                </div> -->

                <div class="content-container">
                    <!-- <div class="left-icon-div green-icon hover-effect"><img src="img/seat.png" class="white-icon2 hover-effect" alt="Basic Plan E" title="Basic Plan E"></div> -->
                    <div class="one-column-white">

                            <!-- <div class="overflow-scroll-div">
                                <table>
                                <tr>
                                    <thead>
                                        <th>No.</th>
                                        <th>Booking ID</th>
                                        <th>Plan</th>
                                    </thead>
                                </tr> -->

                                    <tbody>
                                        <?php
                                        if($bookingDetails)
                                        {
                                            for($cnt = 0;$cnt < count($bookingDetails) ;$cnt++)
                                            {
																							$endDate = $bookingDetails[$cnt]->getEndDate();
																							$duration = $bookingDetails[$cnt]->getDuration();
																							$space = " ";
																							$timeLine = $bookingDetails[$cnt]->getTimeline();
																							$time = $duration . $space . $timeLine;
																							$endDate = date("Y-m-d", strtotime(date("Y-m-d", strtotime($endDate)). "+ $time"));

																							if ($currentDate > $endDate) {
																								?><div class="p-container">
													                                            	<div class="left-content-p">
													                                                    <p class="white-box-content-p hover-effect"><?php echo $bookingDetails[$cnt]->getAreaType();?> | <?php echo $dateEnd = date("d-m-Y",strtotime($bookingDetails[$cnt]->getEndDate()));?></p>
																									</div>
													                                                <!-- <p class="light-grey-text small-date hover-effect"><?php //$dateCreated = date("Y-m-d",strtotime($adminList[$cnt]->getDateCreated()));echo $dateCreated;?></p>
													                                                <p class="white-box-content-p hover-effect"><?php //echo $adminList[$cnt]->getAreaType();?></p> -->

													                                                    <!-- <form action="receipt.php" method="POST"> -->
													                                                    <form action="receiptBookingHistory.php" method="POST" class="right-content-p">
													                                                        <button class="clean receipt-btn" type="submit" name="bookingHistory_ID" value="<?php echo $bookingDetails[$cnt]->getBookingId();?>">
													                                                            <img src="img/receipt.png" class="hover-effect receipt-img" alt="View Receipt" title="View Receipt">

													                                                        </button>
													                                                    </form>

																								</div><?php
																							}
																							?>
                                            <!-- <tr> -->
                                                <!-- <td><?php //echo ($cnt+1)?></td> -->

                                                <!-- <p class="light-grey-text small-date hover-effect">Today    10:00 am - Today 6:00 pm</p>
                                                <p class="white-box-content-p hover-effect">Co-Working Space - No.1</p> -->
<<<<<<< Updated upstream

=======
											<div class="p-container">
                                            	<div class="left-content-p">
                                                    <p class="light-grey-text small-date hover-effect text-break"><?php echo $adminList[$cnt]->getBookingId();?></p>
                                                    <p class="white-box-content-p hover-effect">
                                                        <?php echo $adminList[$cnt]->getAreaType();?> | 
                                                        <?php $dateCreated = date("Y-m-d",strtotime($adminList[$cnt]->getStartDate()));echo $dateCreated;?> |
                                                        <?php echo $adminList[$cnt]->getPaymentVerify();?>
                                                    </p>
												</div>
                                                <!-- <p class="light-grey-text small-date hover-effect"><?php //$dateCreated = date("Y-m-d",strtotime($adminList[$cnt]->getDateCreated()));echo $dateCreated;?></p>
                                                <p class="white-box-content-p hover-effect"><?php //echo $adminList[$cnt]->getAreaType();?></p> -->

                                                    <!-- <form action="receipt.php" method="POST"> -->
                                                    <form action="receiptBookingHistory.php" method="POST" class="right-content-p">
                                                        <button class="clean receipt-btn" type="submit" name="bookingHistory_ID" value="<?php echo $adminList[$cnt]->getBookingId();?>">
                                                            <img src="img/receipt.png" class="hover-effect receipt-img" alt="View History" title="View History">
                                                            
                                                        </button>
                                                    </form>

											</div>
>>>>>>> Stashed changes
                                                <!-- <td><?php //echo $adminList[$cnt]->getBookingId();?></td>
                                                <td><?php //echo $adminList[$cnt]->getAreaType();?></td> -->

                                                <!-- <td>
                                                    <?php //$dateCreated = date("Y-m-d",strtotime($adminList[$cnt]->getDateCreated()));echo $dateCreated;?>
                                                </td> -->
                                                <!-- <td> <?php //echo $adminList[$cnt]->getWithdrawalAmount();?></td> -->
                                            <!-- </tr> -->
                                            <?php
                                            }
                                        }
                                        else
                                        {
                                        ?>
                                            <p>No Booking History...</p>
                                        <?php
                                        }
                                        ?>
                                    <!-- </tbody>
                                </table>
                            </div> -->

                    </div>
                </div>
            </div>
        </div>

        <div class="clear"></div>
        <!--<div class="fillup-leftspace2"></div><a href="addBooking.php"><div class="blue-btn add-new-btn">Add New Booking</div></a>-->
  		<div class="clear"></div>
        <div class="divider"></div>

</div>


<?php include 'js.php'; ?>
</body>
</html>
