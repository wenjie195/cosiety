<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Booking.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $area_type = rewrite($_POST["area_type"]);
    $cost = rewrite($_POST["cost"]);
    $duration = rewrite($_POST["duration"]);
    $start_date = rewrite($_POST["start_date"]);
    $total_ppl = rewrite($_POST["total_ppl"]);
    $order_by = rewrite($_POST["order_by"]);
    $discount = rewrite($_POST["discount"]);
    $total_price = rewrite($_POST["total_price"]);
    $total_seat = rewrite($_POST["total_seat"]);
    $project_title = rewrite($_POST["project_title"]);
    $project_details = rewrite($_POST["project_details"]);
}

$conn->close();

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Payment Method | Cosiety" />
<title>Payment Method | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">

<!-- <form action="utilities/bookingFunction.php" method="POST"> -->
<form action="receipt.php" method="POST">

<!-- <h4> <?php //echo $area_type?> </h4>
<h4> <?php //echo $cost?> </h4>
<h4> <?php //echo $duration?> </h4>
<h4> <?php //echo $start_date?> </h4>
<h4> <?php //echo $total_ppl?> </h4>
<h4> <?php //echo $order_by?> </h4>
<h4> <?php //echo $discount?> </h4>
<h4> <?php //echo $total_price?> </h4>
<h4> <?php //echo $total_seat?> </h4>
<h4> <?php //echo $project_title?> </h4>
<h4> <?php //echo $project_details?> </h4> -->

<!-- <h4> <?php //echo $userDetails->getUsername();?> </h4> -->

	<h1 class="backend-title-h1">Payment</h1>
	<div class="three-div">
    	<p class="grey-text input-top-p">Duration (Month)</p>
        <input class="three-select clean" id="duration" name="duration"  type="number" placeholder="1" >
    </div>
	<div class="three-div middle-three-div second-three-div">
        <p class="grey-text input-top-p">Start Date</p>
        <input class="three-select clean" type="date" id="start_date" name="start_date">
        <!-- <input type="date" class="three-select clean"> -->
    </div>
	<div class="three-div">
    	<p class="grey-text input-top-p">Discount</p>
        <p class="three-select-p">20%</p>
        <input type="hidden" name="discount" id="discount" value="20%">
    </div>
    <div class="tempo-three-clear"></div> 
 	<div class="three-div second-three-div">
    	<p class="grey-text input-top-p">Total</p>
        <p class="total-p">RM639.20</p>
        <input type="hidden" name="total_price" id="total_price" value="RM639.20">
    </div>
    <div class="clear"></div>
	<div class="divider"></div>  


<input type="hidden" name="username" id="username" value="<?php echo $userDetails->getUsername();?>">
<input type="hidden" name="uid" id="uid" value="<?php echo $userDetails->getUid();?>">
<input type="hidden" name="area_type" id="area_type" value="<?php echo $area_type?>">
<input type="hidden" name="cost" id="cost" value="<?php echo $cost?>">
<input type="hidden" name="duration" id="duration" value="<?php echo $duration?>">
<input type="hidden" name="start_date" id="start_date" value="<?php echo $start_date?>">
<input type="hidden" name="total_ppl" id="total_ppl" value="<?php echo $total_ppl?>">
<input type="hidden" name="order_by" id="order_by" value="<?php echo $order_by?>">
<input type="hidden" name="discount" id="discount" value="<?php echo $discount?>">
<input type="hidden" name="total_price" id="total_price" value="<?php echo $total_price?>">
<input type="hidden" name="total_seat" id="total_seat" value="<?php echo $total_seat?>">
<input type="hidden" name="project_title" id="project_title" value="<?php echo $project_title?>">
<input type="hidden" name="project_details" id="project_details" value="<?php echo $project_details?>">
    <!--
    <div class="half-div-radio">
        <label class="container2">
          <div class="payment1-div">
          	<p class="thin-payment-p">One Time Payment (Discount 10%)</p>
            <p class="thick-payment-p">Total: RM1378.00</p>
          </div>
          <input type="radio" checked="checked" name="radio">
          <span class="checkmark2"></span>
        </label>
    </div>
    <div class="half-div-radio">    
        <label class="container2">
          <div class="payment1-div">
          	<p class="thin-payment-p">Monthly Payment (No Extra Discount)</p>
            <p class="thick-payment-p">Total: RM760.00/month</p>
          </div>       
          <input type="radio" name="radio">
          <span class="checkmark2"></span>
        </label>
    </div>-->
    <h2 class="backend-title-h2">Choose Your Payment Method</h2>  
    <div class="three-div">
        <select class="three-select clean" id="payment_method" name="payment_method">
            <option value="ipay88" name="ipay88">ipay88</option>
            <option value="Visa_or_MasterCard" name="Visa_or_MasterCard">Visa_or_MasterCard</option>
            <option value="online_banking" name="online_banking">online_banking</option>
        </select>
    </div>
    <div class="divider"></div><div class="divider"></div>
    <div class="clear"></div>
    <div class="fillup-extra-space"></div><button class="blue-btn payment-button clean next-btn">Next</button>
    <!-- <div class="fillup-extra-space"></div><a href="reserveSpace.php"><button class="blue-btn payment-button clean next-btn">Next</button></a> -->
    <div class="clear"></div>
    
</form>
</div>


<?php include 'js.php'; ?>
</body>
</html>