<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Calendar Settings | Cosiety" />
<title>Calendar Settings | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
<h1 class="backend-title-h1">Calendar Settings</h1>
    <div class="clear"></div>
    <div class="small-divider"></div>
    <div class="width100 overflow">
    	<html ng-app="myApp" ng-controller="AppCtrl" lang="en">
        	<head>
            	<meta charset="utf-8">
                <title>Circle</title>
        	</head>
        	<body>
            	<div calendar class="calendar" id="calendar"></div>
         	</body>
    	</html>       
	</div>
    <div class="clear"></div>
    <div class="divider"></div>    
    <div class="width100">
    	<select class="clean title-select">
        	<option>September</option>
            <option>August</option>
            <option>July</option>
        </select>
    	<div class="overflow-scroll-div">    
            <table class="issue-table">
            	<tr>
                	<thead>
                    	<th>No.</th>
                        <th>Closed Date</th>
                        <th>Closed Day</th>       
                        <th>Remark</th>                                                               
                    </thead>
                </tr>
                <tr data-url="editCalendar.php" class="link-to-details hover-effect">
                	<td>1.</td>
                    <td>6/9/2019</td>
                    <td>Friday</td>  
                    <td>Under construction.</td>                                              
                </tr>
            </table>
		</div>
    </div>
  	<div class="clear"></div>
    <div class="divider"></div>
    <div class="clear"></div>
    <div class="fillup-extra-space"></div><a href="editCalendar.php"><button class="blue-btn payment-button clean next-btn">Edit</button></a>
    <div class="clear"></div>
    	<!--
    
        <div class="clear"></div>
        <div class="fillup-leftspace"></div><a href="addBooking.php"><div class="blue-btn add-new-btn">Add New Booking</div></a>-->
  
     
</div>


<?php include 'js.php'; ?>
</body>
</html>