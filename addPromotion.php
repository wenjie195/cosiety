<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Add Promotion | Cosiety" />
<title>Add Promotion | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1">Add Promotion</h1>
	<div class="edit-half-div">
    	<p class="grey-text input-top-p">Promotion Start Date</p>
        <input class="three-select clean" type="date">
	</div>
	<div class="edit-half-div second-edit-half-div">
    	<p class="grey-text input-top-p">Promotion End Date</p>
        <input class="three-select clean" type="date">
	</div>            
	<div class="clear"></div>
	<div class="edit-half-div">
    	<p class="grey-text input-top-p">Effective Booking Start Date </p>
        <input class="three-select clean" type="date">
	</div>
	<div class="edit-half-div second-edit-half-div">
    	<p class="grey-text input-top-p">Effective Booking End Date </p>
        <input class="three-select clean" type="date">
	</div>            
	<div class="clear"></div>
	<div class="edit-half-div">
    	<p class="grey-text input-top-p">Select Plan</p>
        <select class="three-select clean">
            	<option>Lounge</option>
                <option>Dedicated Work Desk</option>
                <option>Co-Working Space (Hot Seat)</option>
                <option>Private Suit</option>             
        </select>
	</div>
	<div class="edit-half-div second-edit-half-div">
    	<p class="grey-text input-top-p">Min. Duration</p>
        <select class="three-select clean">
            	<option>1 Months</option>
                <option>2 Months</option>
                <option>3 Months</option>
                <option>4 Months</option>
                <option>5 Months</option>              
        </select>
	</div>     
    <div class="clear"></div>   
    	<div class="edit-half-div">
    	<p class="grey-text input-top-p">Min. No. of People</p>
        <select class="three-select clean">
        	<option>-</option>
        	<option>5</option>
            <option>6</option>
            <option>7</option> 
            <option>8</option> 
            <option>9</option>
            <option>10</option>
            <option>12</option> 
            <option>15</option>             
        </select>
	</div>
	<div class="edit-half-div second-edit-half-div">
    	<p class="grey-text input-top-p">Discount (%)</p>
        <input class="three-select clean" type="number">
	</div>    
    <div class="clear"></div>
    <div class="width100 overflow">
            <div class="upload-btn-wrapper">
              <button class="upload-btn">Upload Image</button>
              <input class="hidden-input" type="file" name="myfile" />
            </div>
            <!-- Crop the image 16:9 -->
            <p class="img-preview">Image Preview</p> 
            <div class="left-img-preview"><img src="img/promotion.jpg" class="uploaded-img"></div><span class="right-remove-span hover-effect">Remove</span>           
    </div> 
    <div class="clear"></div> 
	<div class="divider"></div>
    <div class="fillup-extra-space"></div><a href="promotion.php"><button class="blue-btn payment-button clean next-btn">Confirm</button></a>
    <div class="clear"></div>
    <div class="fillup-extra-space2"></div><a  onclick="goBack()" class="cancel-a hover-effect">Cancel</a>
</div>


<?php include 'js.php'; ?>
</body>
</html>