-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 06, 2019 at 09:10 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_cosiety`
--

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `booking_id` varchar(255) DEFAULT NULL,
  `area_type` varchar(255) DEFAULT NULL,
  `cost` decimal(50,2) DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `timeline` text DEFAULT NULL,
  `start_date` timestamp(3) NULL DEFAULT NULL,
  `end_date` timestamp(1) NULL DEFAULT NULL,
  `total_ppl` varchar(255) DEFAULT NULL,
  `order_by` varchar(255) DEFAULT NULL,
  `discount` varchar(255) DEFAULT NULL,
  `total_price` decimal(50,2) DEFAULT NULL,
  `total_seat` varchar(255) DEFAULT NULL,
  `project_title` varchar(255) DEFAULT NULL,
  `project_details` varchar(255) DEFAULT NULL,
  `payment_method` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `uid`, `booking_id`, `area_type`, `cost`, `duration`, `timeline`, `start_date`, `end_date`, `total_ppl`, `order_by`, `discount`, `total_price`, `total_seat`, `project_title`, `project_details`, `payment_method`, `date_created`, `date_updated`) VALUES
(100, 'c875c4116e8343e728db0a439b2097ee', 'ae76434893e6737babb3ca2cddc6b082', 'Dedicated Work Desk', '799.00', '1', 'Month', '2019-12-05 16:00:00.000', '2019-01-05 16:00:00.0', '1', 'admin', '20', '639.20', '1', 'vida', 'tech', 'Online Banking', '2019-12-06 07:26:53', '2019-12-06 07:53:06'),
(102, 'c875c4116e8343e728db0a439b2097ee', '32d26b599ca2be7c04166e225a69581a', 'Daily Pass', '30.00', '1', 'Day', '2019-12-05 16:00:00.000', '2018-12-06 16:00:00.0', '1', 'admin', '20', '24.00', NULL, NULL, NULL, 'Online Banking', '2019-12-06 07:44:45', '2019-12-06 08:01:14'),
(103, 'c875c4116e8343e728db0a439b2097ee', '8e76780ba4ca2e1d59d9902d98d1cbcb', 'Dedicated Work Desk', '799.00', '1', 'Month', '2019-12-05 16:00:00.000', '2020-01-05 16:00:00.0', '1', 'admin', '20', '639.20', '1', '', '', 'Online Banking', '2019-12-06 08:01:51', '2019-12-06 08:01:51');

-- --------------------------------------------------------

--
-- Table structure for table `booking_lounge`
--

CREATE TABLE `booking_lounge` (
  `id` bigint(20) NOT NULL,
  `area_type` varchar(255) DEFAULT NULL,
  `start_date` timestamp(1) NULL DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `end_date` varchar(255) DEFAULT NULL,
  `payment_amount` decimal(50,2) DEFAULT NULL,
  `payment_method` varchar(255) DEFAULT NULL,
  `payment_verify` varchar(255) DEFAULT NULL,
  `orderBy` varchar(255) DEFAULT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT current_timestamp(),
  `dateUpdated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `booking_lounge`
--

INSERT INTO `booking_lounge` (`id`, `area_type`, `start_date`, `duration`, `end_date`, `payment_amount`, `payment_method`, `payment_verify`, `orderBy`, `dateCreated`, `dateUpdated`) VALUES
(30, 'Yearly Membership', '2019-12-05 16:00:00.0', '1', '2020-12-06', '1999.00', 'Online Banking', NULL, 'admin', '2019-12-06 07:09:46', '2019-12-06 07:09:46'),
(31, 'Daily Pass', '2019-12-05 16:00:00.0', '1', '2019-12-07', '30.00', 'Online Banking', NULL, 'admin', '2019-12-06 07:44:45', '2019-12-06 07:44:45');

-- --------------------------------------------------------

--
-- Table structure for table `booking_meeting`
--

CREATE TABLE `booking_meeting` (
  `id` bigint(20) NOT NULL,
  `seat_id` varchar(255) DEFAULT NULL,
  `seat_status` int(2) NOT NULL DEFAULT 0 COMMENT '0=available, 1=booked',
  `start_date` timestamp(1) NULL DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `end_date` varchar(255) DEFAULT NULL,
  `payment_amount` varchar(255) DEFAULT NULL,
  `payment_verify` varchar(255) DEFAULT NULL,
  `orderBy` varchar(255) DEFAULT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT current_timestamp(),
  `dateUpdated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `booking_private`
--

CREATE TABLE `booking_private` (
  `id` bigint(20) NOT NULL,
  `seat_id` varchar(255) DEFAULT NULL,
  `seat_status` int(2) NOT NULL DEFAULT 0 COMMENT '0=available, 1=booked',
  `start_date` timestamp(1) NULL DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `end_date` varchar(255) DEFAULT NULL,
  `payment_amount` decimal(50,2) DEFAULT NULL,
  `payment_verify` varchar(255) DEFAULT NULL,
  `orderBy` varchar(255) DEFAULT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT current_timestamp(),
  `dateUpdated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `booking_private`
--

INSERT INTO `booking_private` (`id`, `seat_id`, `seat_status`, `start_date`, `duration`, `end_date`, `payment_amount`, `payment_verify`, `orderBy`, `dateCreated`, `dateUpdated`) VALUES
(1, '1', 1, '2019-12-05 16:00:00.0', '1', '2020-01-06', '800.00', NULL, 'admin', '2019-12-01 02:32:16', '2019-12-06 07:14:12'),
(2, '2', 1, '2019-12-04 05:20:00.0', '1', NULL, '1280.00', NULL, 'testing', '2019-12-01 02:32:16', '2019-12-04 05:21:38'),
(3, '3', 1, '2019-12-04 08:06:00.0', '1', NULL, '5120.00', NULL, 'testing', '2019-12-01 02:34:03', '2019-12-04 08:06:34'),
(4, '4', 1, '2019-12-04 16:00:00.0', '1', NULL, '1600.00', NULL, 'testing', '2019-12-01 02:34:03', '2019-12-05 04:24:39'),
(5, '5', 1, '2019-12-04 16:00:00.0', '1', NULL, '1600.00', NULL, 'testing', '2019-12-01 02:34:14', '2019-12-05 04:24:39'),
(6, '6', 1, '2019-12-04 16:00:00.0', '1', NULL, '1600.00', NULL, 'testing', '2019-12-01 02:34:14', '2019-12-05 04:28:18'),
(7, '7', 1, '2019-12-04 16:00:00.0', '1', NULL, '1600.00', NULL, 'testing', '2019-12-01 02:34:22', '2019-12-05 04:28:18'),
(8, '8', 1, '2019-12-04 16:00:00.0', '1', NULL, '1600.00', NULL, 'testing', '2019-12-01 02:34:22', '2019-12-05 04:34:05'),
(9, '9', 1, '2019-12-04 16:00:00.0', '1', NULL, '15360.00', NULL, 'testing', '2019-12-01 02:34:40', '2019-12-05 04:40:02'),
(10, '10', 1, '2019-12-04 16:00:00.0', '1', NULL, '15360.00', NULL, 'testing', '2019-12-01 02:34:40', '2019-12-05 04:40:02'),
(11, '11', 1, '2019-12-04 16:00:00.0', '1', NULL, '15360.00', NULL, 'testing', '2019-12-01 02:35:17', '2019-12-05 04:40:02'),
(12, '12', 1, '2019-12-04 16:00:00.0', '1', NULL, '2400.00', NULL, 'testing', '2019-12-01 02:35:17', '2019-12-05 04:31:00'),
(13, '13', 1, '2019-12-04 16:00:00.0', '1', NULL, '2400.00', NULL, 'testing', '2019-12-01 02:35:26', '2019-12-05 04:31:00'),
(21, '14', 1, '2019-12-04 16:00:00.0', '1', NULL, '2400.00', NULL, 'testing', '2019-12-01 02:35:17', '2019-12-05 04:31:00'),
(22, '15', 1, '2019-12-04 16:00:00.0', '1', NULL, '1600.00', NULL, 'testing', '2019-12-01 02:35:17', '2019-12-05 04:34:05'),
(23, '16', 1, '2019-12-04 05:20:00.0', '1', NULL, '1280.00', NULL, 'testing', '2019-12-01 02:35:26', '2019-12-04 05:21:38');

-- --------------------------------------------------------

--
-- Table structure for table `booking_workdesk`
--

CREATE TABLE `booking_workdesk` (
  `id` bigint(20) NOT NULL,
  `seat_id` varchar(255) DEFAULT NULL,
  `seat_status` int(2) NOT NULL DEFAULT 0 COMMENT '0=available, 1=booked',
  `start_date` timestamp(1) NULL DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `end_date` varchar(255) DEFAULT NULL,
  `payment_amount` decimal(50,2) DEFAULT NULL,
  `payment_verify` varchar(255) DEFAULT NULL,
  `orderBy` varchar(255) DEFAULT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT current_timestamp(),
  `dateUpdated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `booking_workdesk`
--

INSERT INTO `booking_workdesk` (`id`, `seat_id`, `seat_status`, `start_date`, `duration`, `end_date`, `payment_amount`, `payment_verify`, `orderBy`, `dateCreated`, `dateUpdated`) VALUES
(1, '1', 1, '2019-12-05 16:00:00.0', '1', '2020-01-06', '639.20', NULL, 'admin', '2019-12-01 10:32:16', '2019-12-06 08:01:51'),
(2, '2', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 10:32:16', '2019-12-06 07:57:04'),
(3, '3', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 10:34:03', '2019-12-06 04:17:00'),
(4, '4', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 10:34:03', '2019-12-06 04:17:30'),
(5, '5', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 10:34:14', '2019-12-06 04:17:14'),
(6, '6', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 10:34:14', '2019-12-06 04:17:00'),
(7, '7', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 10:34:22', '2019-12-06 04:17:00'),
(8, '8', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 10:34:22', '2019-12-06 04:17:00'),
(9, '9', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 10:34:40', '2019-12-06 04:17:00'),
(10, '10', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 10:34:40', '2019-12-04 03:16:25'),
(11, '11', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 10:35:17', '2019-12-04 03:16:27'),
(12, '12', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 10:35:17', '2019-12-04 03:15:30'),
(13, '13', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 10:35:26', '2019-12-01 10:35:26'),
(21, '14', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 10:35:17', '2019-12-06 04:20:26'),
(22, '15', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 10:35:17', '2019-12-06 04:20:26'),
(23, '16', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 10:35:26', '2019-12-06 04:25:17');

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `username` varchar(200) DEFAULT NULL COMMENT 'account that login by user to make order',
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_holder` varchar(255) DEFAULT NULL,
  `bank_account_no` int(255) DEFAULT NULL,
  `receipt` varchar(255) DEFAULT 'NULL',
  `name` varchar(200) DEFAULT NULL COMMENT 'person to receive the product for delivery',
  `contactNo` varchar(20) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `address_line_1` varchar(2500) DEFAULT NULL,
  `address_line_2` varchar(2500) DEFAULT NULL,
  `address_line_3` varchar(2500) DEFAULT NULL,
  `city` varchar(500) DEFAULT NULL,
  `zipcode` varchar(100) DEFAULT NULL,
  `state` varchar(500) DEFAULT NULL,
  `country` varchar(500) DEFAULT NULL,
  `subtotal` decimal(50,0) DEFAULT NULL,
  `total` decimal(50,0) DEFAULT NULL COMMENT 'include postage fees',
  `payment_method` varchar(255) DEFAULT NULL,
  `payment_amount` int(255) DEFAULT NULL,
  `payment_bankreference` varchar(255) DEFAULT NULL,
  `payment_date` varchar(20) DEFAULT NULL,
  `payment_time` varchar(20) DEFAULT NULL,
  `payment_status` varchar(200) DEFAULT NULL COMMENT 'pending, accepted/completed, rejected, NULL = nothing',
  `shipping_status` varchar(200) DEFAULT NULL COMMENT 'pending, shipped, refunded',
  `shipping_method` varchar(200) DEFAULT NULL,
  `shipping_date` date DEFAULT NULL,
  `tracking_number` varchar(200) DEFAULT NULL,
  `reject_reason` varchar(255) DEFAULT NULL,
  `refund_method` varchar(255) DEFAULT NULL,
  `refund_amount` int(255) DEFAULT NULL,
  `refund_note` varchar(255) DEFAULT NULL,
  `refund_reason` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`id`, `uid`, `username`, `bank_name`, `bank_account_holder`, `bank_account_no`, `receipt`, `name`, `contactNo`, `email`, `address_line_1`, `address_line_2`, `address_line_3`, `city`, `zipcode`, `state`, `country`, `subtotal`, `total`, `payment_method`, `payment_amount`, `payment_bankreference`, `payment_date`, `payment_time`, `payment_status`, `shipping_status`, `shipping_method`, `shipping_date`, `tracking_number`, `reject_reason`, `refund_method`, `refund_amount`, `refund_note`, `refund_reason`, `date_created`, `date_updated`) VALUES
(1, 'eb3b449f0c4868acf7cebaa02887a3e6', 'testing', NULL, NULL, NULL, 'maxresdefault.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-05 09:49:31', '2019-12-05 09:49:31'),
(2, 'c875c4116e8343e728db0a439b2097ee', 'admin', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-06 07:13:39', '2019-12-06 07:13:39'),
(3, 'c875c4116e8343e728db0a439b2097ee', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-06 07:14:12', '2019-12-06 07:14:12'),
(4, 'c875c4116e8343e728db0a439b2097ee', 'admin', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-06 07:25:05', '2019-12-06 07:25:05'),
(5, 'c875c4116e8343e728db0a439b2097ee', 'admin', NULL, NULL, NULL, 'SeverusSnape.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-06 07:26:53', '2019-12-06 07:26:53'),
(6, 'c875c4116e8343e728db0a439b2097ee', 'admin', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-06 07:44:15', '2019-12-06 07:44:15'),
(7, 'c875c4116e8343e728db0a439b2097ee', 'admin', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-06 08:01:52', '2019-12-06 08:01:52');

-- --------------------------------------------------------

--
-- Table structure for table `room_price`
--

CREATE TABLE `room_price` (
  `id` int(255) NOT NULL,
  `type_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `display` smallint(1) NOT NULL DEFAULT 1,
  `price` decimal(50,2) NOT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `discount` int(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `room_price`
--

INSERT INTO `room_price` (`id`, `type_id`, `type`, `name`, `display`, `price`, `duration`, `discount`, `description`) VALUES
(1, 1, 'Lounge', 'Yearly Membership', 1, '1999.00', 'Year', 20, 'RM999 only if sign up on Nov 2019'),
(2, 1, 'Lounge', 'Monthly Membership', 1, '199.00', 'Month', 20, 'RM99 only if sign up on Nov 2019\r\n'),
(3, 1, 'Lounge', 'Daily Pass', 1, '30.00', 'Day', 20, 'RM20 only if sign up on Nov 2019\r\n'),
(4, 2, 'Work Desk', 'Dedicated Work Desk', 1, '799.00', 'Month', 20, 'Enjoy 20% off from the monthly rental\r\n\r\n'),
(5, 2, 'Work Desk', 'Co-Working Space (Hot Seat)', 1, '399.00', 'Month', 20, 'Enjoy 20% off from the monthly rental'),
(6, 3, 'Private Suit', '1 Work Station', 1, '1000.00', 'Month', 20, 'Enjoy 20% off from the monthly rental'),
(7, 3, 'Private Suit', '2 Work Station', 1, '1600.00', 'Month', 20, 'Enjoy 20% off from the monthly rental'),
(8, 3, 'Private Suit', '3 Work Stations', 1, '2400.00', 'Month', 20, 'Enjoy 20% off from the monthly rental'),
(9, 3, 'Private Suit', '4 Work Stations', 1, '3200.00', 'Month', 20, 'Enjoy 20% off from the monthly rental'),
(10, 3, 'Private Suit', '5 Work Stations', 1, '4000.00', 'Month', 20, 'Enjoy 20% off from the monthly rental'),
(11, 3, 'Private Suit', '6 Work Stations', 1, '4800.00', 'Month', 20, 'Enjoy 20% off from the monthly rental'),
(12, 3, 'Private Suit', '7 Work Stations', 1, '5600.00', 'Month', 20, 'Enjoy 20% off from the monthly rental'),
(13, 3, 'Private Suit', '8 Work Stations', 1, '6400.00', 'Month', 20, 'Enjoy 20% off from the monthly rental'),
(14, 4, 'Meeting Room', 'Meeting Room', 1, '0.00', 'Hour', 20, 'Comfortable environment'),
(62810849, 1, 'Lounge', 'Severus', 0, '50.00', 'Hour', 0, 'Try me');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `uid` varchar(255) NOT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL COMMENT 'For login probably if needed',
  `email` varchar(200) DEFAULT NULL COMMENT 'Can login with email too',
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `ic_no` varchar(200) DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `login_type` int(2) NOT NULL DEFAULT 1 COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `birthday` date DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`uid`, `username`, `email`, `password`, `salt`, `phone_no`, `ic_no`, `full_name`, `country`, `login_type`, `user_type`, `date_created`, `date_updated`, `birthday`, `gender`) VALUES
('68a18ec9b977f0aa47a901fccdd666d4', 'aaaaaa', 'aa@gg.cc', 'd29a1fbf3d0e24f0a71c5f849d78cf66f71f4af086efda7ba968718072eeca53', 'ea9158d9a55b9404ef39423715c3245020fd1d9b', '111111', NULL, NULL, 'Malaysia', 1, 1, '2019-11-01 03:45:33', '2019-11-01 03:45:33', NULL, NULL),
('afac5eac642581a6ae8523f7623de49a', 'lolo', 'lolo@gg.nm', 'd1e7fa5b84cb55554724f777b9af5ec32fd4ed7853c25ebfc85702625f696281', '87ef210d9e902a1f9f90bb1827df4af5302799e0', '14141414', NULL, NULL, 'Malaysia', 1, 1, '2019-11-01 05:01:54', '2019-11-01 05:01:54', NULL, NULL),
('c875c4116e8343e728db0a439b2097ee', 'admin', 'admin@gmail.com', '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '1233214455', NULL, NULL, 'Malaysia', 1, 0, '2019-11-01 03:26:22', '2019-11-28 03:24:28', NULL, NULL),
('eb3b449f0c4868acf7cebaa02887a3e6', 'testing', 'test@gg.cc', '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '111222555', NULL, NULL, 'Singapore', 1, 1, '2019-10-31 03:51:51', '2019-11-28 03:24:19', NULL, NULL),
('f7c8cf4afec0dce4ccf677ac81661dec', 'zzzzz', 'zzz@gg.zz', '4f150972c7cef5240558a81dd6e136c13620defb6782a2fa05eeb71db0492ae6', 'a21926404c1d56d28829b210f76777c988b9f38b', '22222222', NULL, NULL, 'Singapore', 1, 1, '2019-10-31 09:33:15', '2019-10-31 09:33:15', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bookingUid_to_userUsername` (`uid`);

--
-- Indexes for table `booking_lounge`
--
ALTER TABLE `booking_lounge`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking_meeting`
--
ALTER TABLE `booking_meeting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking_private`
--
ALTER TABLE `booking_private`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking_workdesk`
--
ALTER TABLE `booking_workdesk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room_price`
--
ALTER TABLE `room_price`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`uid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT for table `booking_lounge`
--
ALTER TABLE `booking_lounge`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `booking_meeting`
--
ALTER TABLE `booking_meeting`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `booking_private`
--
ALTER TABLE `booking_private`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `booking_workdesk`
--
ALTER TABLE `booking_workdesk`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `room_price`
--
ALTER TABLE `room_price`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62810850;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `booking`
--
ALTER TABLE `booking`
  ADD CONSTRAINT `bookingUid_to_userUsername` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
