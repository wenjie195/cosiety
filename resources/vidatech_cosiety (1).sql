-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 28, 2019 at 04:24 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_cosiety`
--

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `booking_id` varchar(255) DEFAULT NULL,
  `area_type` varchar(255) DEFAULT NULL,
  `cost` varchar(255) DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `start_date` varchar(255) DEFAULT NULL,
  `total_ppl` varchar(255) DEFAULT NULL,
  `order_by` varchar(255) DEFAULT NULL,
  `discount` varchar(255) DEFAULT NULL,
  `total_price` varchar(255) DEFAULT NULL,
  `total_seat` varchar(255) DEFAULT NULL,
  `project_title` varchar(255) DEFAULT NULL,
  `project_details` varchar(255) DEFAULT NULL,
  `payment_method` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `uid`, `booking_id`, `area_type`, `cost`, `duration`, `start_date`, `total_ppl`, `order_by`, `discount`, `total_price`, `total_seat`, `project_title`, `project_details`, `payment_method`, `date_created`, `date_updated`) VALUES
(1, 'f7c8cf4afec0dce4ccf677ac81661dec', '1d8ec854aa33ea162f7290c24cc04fba', 'Dedicated Work Desk', 'RM799.00', '1 month', '2019-11-07', '5', 'Personal', '20%', 'RM957.60', '1', 'asd asd test', 'test 123', 'test 123', '2019-11-01 03:08:14', '2019-11-01 03:08:14'),
(2, '68a18ec9b977f0aa47a901fccdd666d4', '19fc10f13d8d8d8d746eddbc357284ef', 'Dedicated Work Desk', 'RM799.00', '2 month', '2019-11-01', '6', 'XXX Company', '20%', 'RM957.60', '4', 'asdf', 'asdsaf', 'asdsaf', '2019-11-01 03:48:09', '2019-11-01 03:48:09'),
(11, 'afac5eac642581a6ae8523f7623de49a', '9a7ec6ffda648e1544689777f2415a71', 'Dedicated Work Desk', 'RM799.00', '2 month', '2019-11-15', '4', 'Personal', '20%', 'RM957.60', '3', 'lololo', 'yt young', 'ipay88', '2019-11-01 06:09:14', '2019-11-01 06:09:14'),
(12, 'eb3b449f0c4868acf7cebaa02887a3e6', '7c26f3c33dac55d560365262311666cb', 'Dedicated Work Desk', 'RM799.00', '1 month', '2019-11-09', '3', 'XXX Company', '20%', 'RM639.20', '1', 'asd', 'qqwwerr', 'ipay88', '2019-11-01 06:39:19', '2019-11-01 06:39:19');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `uid` varchar(255) NOT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL COMMENT 'For login probably if needed',
  `email` varchar(200) DEFAULT NULL COMMENT 'Can login with email too',
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `ic_no` varchar(200) DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `login_type` int(2) NOT NULL DEFAULT 1 COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `birthday` date DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`uid`, `username`, `email`, `password`, `salt`, `phone_no`, `ic_no`, `full_name`, `country`, `login_type`, `user_type`, `date_created`, `date_updated`, `birthday`, `gender`) VALUES
('68a18ec9b977f0aa47a901fccdd666d4', 'aaaaaa', 'aa@gg.cc', 'd29a1fbf3d0e24f0a71c5f849d78cf66f71f4af086efda7ba968718072eeca53', 'ea9158d9a55b9404ef39423715c3245020fd1d9b', '111111', NULL, NULL, 'Malaysia', 1, 1, '2019-11-01 03:45:33', '2019-11-01 03:45:33', NULL, NULL),
('afac5eac642581a6ae8523f7623de49a', 'lolo', 'lolo@gg.nm', 'd1e7fa5b84cb55554724f777b9af5ec32fd4ed7853c25ebfc85702625f696281', '87ef210d9e902a1f9f90bb1827df4af5302799e0', '14141414', NULL, NULL, 'Malaysia', 1, 1, '2019-11-01 05:01:54', '2019-11-01 05:01:54', NULL, NULL),
('c875c4116e8343e728db0a439b2097ee', 'admin', 'admin@gmail.com', '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '1233214455', NULL, NULL, 'Malaysia', 1, 0, '2019-11-01 03:26:22', '2019-11-28 03:24:28', NULL, NULL),
('eb3b449f0c4868acf7cebaa02887a3e6', 'testing', 'test@gg.cc', '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '111222555', NULL, NULL, 'Singapore', 1, 1, '2019-10-31 03:51:51', '2019-11-28 03:24:19', NULL, NULL),
('f7c8cf4afec0dce4ccf677ac81661dec', 'zzzzz', 'zzz@gg.zz', '4f150972c7cef5240558a81dd6e136c13620defb6782a2fa05eeb71db0492ae6', 'a21926404c1d56d28829b210f76777c988b9f38b', '22222222', NULL, NULL, 'Singapore', 1, 1, '2019-10-31 09:33:15', '2019-10-31 09:33:15', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bookingUid_to_userUsername` (`uid`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`uid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `booking`
--
ALTER TABLE `booking`
  ADD CONSTRAINT `bookingUid_to_userUsername` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
