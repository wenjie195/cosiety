-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 03, 2019 at 04:50 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_cosiety`
--

-- --------------------------------------------------------

--
-- Table structure for table `booking_workdesk`
--

CREATE TABLE `booking_workdesk` (
  `id` bigint(20) NOT NULL,
  `seat_id` varchar(255) DEFAULT NULL,
  `seat_status` int(2) NOT NULL DEFAULT 0 COMMENT '0=available, 1=booked',
  `start_date` varchar(255) DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `end_date` varchar(255) DEFAULT NULL,
  `payment_amount` varchar(255) DEFAULT NULL,
  `payment_verify` varchar(255) DEFAULT NULL,
  `orderBy` varchar(255) DEFAULT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT current_timestamp(),
  `dateUpdated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `booking_workdesk`
--

INSERT INTO `booking_workdesk` (`id`, `seat_id`, `seat_status`, `start_date`, `duration`, `end_date`, `payment_amount`, `payment_verify`, `orderBy`, `dateCreated`, `dateUpdated`) VALUES
(1, '1', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 18:32:16', '2019-12-01 18:32:16'),
(2, '2', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 18:32:16', '2019-12-01 18:32:16'),
(3, '3', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 18:34:03', '2019-12-01 18:34:03'),
(4, '4', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 18:34:03', '2019-12-01 18:34:03'),
(5, '5', 1, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 18:34:14', '2019-12-02 09:21:58'),
(6, '6', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 18:34:14', '2019-12-01 18:34:14'),
(7, '7', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 18:34:22', '2019-12-01 18:34:22'),
(8, '8', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 18:34:22', '2019-12-01 18:34:22'),
(9, '9', 1, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 18:34:40', '2019-12-02 09:22:04'),
(10, '10', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 18:34:40', '2019-12-01 18:34:40'),
(11, '11', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 18:35:17', '2019-12-01 18:35:17'),
(12, '12', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 18:35:17', '2019-12-01 18:35:17'),
(13, '13', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 18:35:26', '2019-12-01 18:35:26'),
(14, '14', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 18:35:26', '2019-12-01 18:35:26'),
(15, '15', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 18:35:35', '2019-12-01 18:35:35'),
(16, '16', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 18:35:35', '2019-12-01 18:35:35');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking_workdesk`
--
ALTER TABLE `booking_workdesk`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking_workdesk`
--
ALTER TABLE `booking_workdesk`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
