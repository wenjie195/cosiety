-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 06, 2019 at 10:49 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_cosiety`
--

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `booking_id` varchar(255) DEFAULT NULL,
  `area_type` varchar(255) DEFAULT NULL,
  `cost` decimal(50,2) DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `start_date` timestamp(3) NULL DEFAULT NULL,
  `end_date` varchar(255) DEFAULT NULL,
  `total_ppl` varchar(255) DEFAULT NULL,
  `order_by` varchar(255) DEFAULT NULL,
  `discount` varchar(255) DEFAULT NULL,
  `total_price` decimal(50,2) DEFAULT NULL,
  `payment_amount` decimal(50,2) DEFAULT NULL,
  `payment_verify` varchar(255) DEFAULT NULL,
  `total_seat` varchar(255) DEFAULT NULL,
  `project_title` varchar(255) DEFAULT NULL,
  `project_details` varchar(255) DEFAULT NULL,
  `payment_method` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `uid`, `booking_id`, `area_type`, `cost`, `duration`, `start_date`, `end_date`, `total_ppl`, `order_by`, `discount`, `total_price`, `payment_amount`, `payment_verify`, `total_seat`, `project_title`, `project_details`, `payment_method`, `date_created`, `date_updated`) VALUES
(48, 'eb3b449f0c4868acf7cebaa02887a3e6', '51c15b4050373f044c35438950d91dca', '3 Work Stations', '2400.00', '1', '2019-12-07 16:00:00.000', NULL, '3', 'testing', '20', '1920.00', NULL, NULL, '3', '', '', 'Online Banking', '2019-12-06 06:33:35', '2019-12-06 06:33:35'),
(49, '68a18ec9b977f0aa47a901fccdd666d4', '85bcb714d902c1ae2231c4230e85a573', '5 Work Stations', '8000.00', '2', '2019-12-18 16:00:00.000', '2020-02-19', '5', 'aaaaaa', '20', '6400.00', '600.00', 'REJECTED', '4', '', '', 'Online Banking', '2019-12-06 08:10:05', '2019-12-06 09:41:08');

-- --------------------------------------------------------

--
-- Table structure for table `booking_lounge`
--

CREATE TABLE `booking_lounge` (
  `id` bigint(20) NOT NULL,
  `area_type` varchar(255) DEFAULT NULL,
  `start_date` timestamp(1) NULL DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `end_date` varchar(255) DEFAULT NULL,
  `payment_amount` decimal(50,2) DEFAULT NULL,
  `payment_method` varchar(255) DEFAULT NULL,
  `payment_verify` varchar(255) DEFAULT NULL,
  `orderBy` varchar(255) DEFAULT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT current_timestamp(),
  `dateUpdated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `booking_private`
--

CREATE TABLE `booking_private` (
  `id` bigint(20) NOT NULL,
  `seat_id` varchar(255) DEFAULT NULL,
  `seat_status` int(2) NOT NULL DEFAULT 0 COMMENT '0=available, 1=booked',
  `start_date` timestamp(1) NULL DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `end_date` varchar(255) DEFAULT NULL,
  `total` decimal(50,2) DEFAULT NULL,
  `payment_amount` decimal(50,2) DEFAULT NULL,
  `payment_time` varchar(255) DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `payment_verify` varchar(255) DEFAULT NULL,
  `orderBy` varchar(255) DEFAULT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT current_timestamp(),
  `dateUpdated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `booking_private`
--

INSERT INTO `booking_private` (`id`, `seat_id`, `seat_status`, `start_date`, `duration`, `end_date`, `total`, `payment_amount`, `payment_time`, `reference`, `payment_verify`, `orderBy`, `dateCreated`, `dateUpdated`) VALUES
(1, '1', 1, '2019-12-07 16:00:00.0', '1', '2020-01-08', '1920.00', '1920.00', '2019-12-06T14:33', 'TESTPAY2331920', NULL, 'testing', '2019-12-01 02:32:16', '2019-12-06 06:33:35'),
(2, '2', 0, NULL, NULL, NULL, '0.00', NULL, NULL, NULL, NULL, NULL, '2019-12-01 02:32:16', '2019-12-06 02:09:07'),
(3, '3', 1, '2019-12-07 16:00:00.0', '1', '2020-01-08', '1920.00', '1920.00', '2019-12-06T14:33', 'TESTPAY2331920', NULL, 'testing', '2019-12-01 02:34:03', '2019-12-06 06:33:35'),
(4, '4', 0, NULL, NULL, NULL, '0.00', NULL, NULL, NULL, NULL, NULL, '2019-12-01 02:34:03', '2019-12-03 19:16:09'),
(5, '5', 1, '2019-12-07 16:00:00.0', '1', '2020-01-08', '1920.00', '1920.00', '2019-12-06T14:33', 'TESTPAY2331920', NULL, 'testing', '2019-12-01 02:34:14', '2019-12-06 06:33:35'),
(6, '6', 0, NULL, NULL, NULL, '0.00', NULL, NULL, NULL, NULL, NULL, '2019-12-01 02:34:14', '2019-12-06 02:09:12'),
(7, '7', 1, '2019-12-18 16:00:00.0', '2', '2020-02-19', '6400.00', '6400.00', '2019-12-06T16:09', '6400TESTAA', 'PENDING', 'aaaaaa', '2019-12-01 02:34:22', '2019-12-06 08:10:05'),
(8, '8', 1, '2019-12-18 16:00:00.0', '2', '2020-02-19', '6400.00', '6400.00', '2019-12-06T16:09', '6400TESTAA', 'PENDING', 'aaaaaa', '2019-12-01 02:34:22', '2019-12-06 08:10:05'),
(9, '9', 1, '2019-12-18 16:00:00.0', '2', '2020-02-19', '6400.00', '6400.00', '2019-12-06T16:09', '6400TESTAA', 'PENDING', 'aaaaaa', '2019-12-01 02:34:40', '2019-12-06 08:10:05'),
(10, '10', 1, '2019-12-18 16:00:00.0', '2', '2020-02-19', '6400.00', '6400.00', '2019-12-06T16:09', '6400TESTAA', 'PENDING', 'aaaaaa', '2019-12-01 02:34:40', '2019-12-06 08:10:05'),
(11, '11', 0, NULL, NULL, NULL, '0.00', NULL, NULL, NULL, NULL, NULL, '2019-12-01 02:35:17', '2019-12-03 19:16:27'),
(12, '12', 0, NULL, NULL, NULL, '0.00', NULL, NULL, NULL, NULL, NULL, '2019-12-01 02:35:17', '2019-12-03 19:15:30'),
(13, '13', 0, NULL, NULL, NULL, '0.00', NULL, NULL, NULL, NULL, NULL, '2019-12-01 02:35:26', '2019-12-01 02:35:26'),
(21, '14', 0, NULL, NULL, NULL, '0.00', NULL, NULL, NULL, NULL, NULL, '2019-12-01 02:35:17', '2019-12-03 19:16:27'),
(22, '15', 0, NULL, NULL, NULL, '0.00', NULL, NULL, NULL, NULL, NULL, '2019-12-01 02:35:17', '2019-12-03 19:15:30'),
(23, '16', 0, NULL, NULL, NULL, '0.00', NULL, NULL, NULL, NULL, NULL, '2019-12-01 02:35:26', '2019-12-06 08:09:28');

-- --------------------------------------------------------

--
-- Table structure for table `booking_workdesk`
--

CREATE TABLE `booking_workdesk` (
  `id` bigint(20) NOT NULL,
  `seat_id` varchar(255) DEFAULT NULL,
  `seat_status` int(2) NOT NULL DEFAULT 0 COMMENT '0=available, 1=booked',
  `start_date` timestamp(1) NULL DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `end_date` varchar(255) DEFAULT NULL,
  `payment_amount` decimal(50,2) DEFAULT NULL,
  `payment_verify` varchar(255) DEFAULT NULL,
  `orderBy` varchar(255) DEFAULT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT current_timestamp(),
  `dateUpdated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `booking_workdesk`
--

INSERT INTO `booking_workdesk` (`id`, `seat_id`, `seat_status`, `start_date`, `duration`, `end_date`, `payment_amount`, `payment_verify`, `orderBy`, `dateCreated`, `dateUpdated`) VALUES
(1, '1', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 10:32:16', '2019-12-06 02:06:41'),
(2, '2', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 10:32:16', '2019-12-06 02:06:38'),
(3, '3', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 10:34:03', '2019-12-04 03:16:05'),
(4, '4', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 10:34:03', '2019-12-04 03:16:09'),
(5, '5', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 10:34:14', '2019-12-04 03:16:11'),
(6, '6', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 10:34:14', '2019-12-04 03:16:14'),
(7, '7', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 10:34:22', '2019-12-04 03:16:17'),
(8, '8', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 10:34:22', '2019-12-04 03:16:20'),
(9, '9', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 10:34:40', '2019-12-04 03:16:22'),
(10, '10', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 10:34:40', '2019-12-04 03:16:25'),
(11, '11', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 10:35:17', '2019-12-04 03:16:27'),
(12, '12', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 10:35:17', '2019-12-04 03:15:30'),
(13, '13', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 10:35:26', '2019-12-01 10:35:26'),
(21, '14', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 10:35:17', '2019-12-04 03:16:27'),
(22, '15', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 10:35:17', '2019-12-04 03:15:30'),
(23, '16', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 10:35:26', '2019-12-06 02:06:25');

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `username` varchar(200) DEFAULT NULL COMMENT 'account that login by user to make order',
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_holder` varchar(255) DEFAULT NULL,
  `bank_account_no` int(255) DEFAULT NULL,
  `receipt` varchar(255) DEFAULT 'NULL',
  `name` varchar(200) DEFAULT NULL COMMENT 'person to receive the product for delivery',
  `contactNo` varchar(20) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `address_line_1` varchar(2500) DEFAULT NULL,
  `address_line_2` varchar(2500) DEFAULT NULL,
  `address_line_3` varchar(2500) DEFAULT NULL,
  `city` varchar(500) DEFAULT NULL,
  `zipcode` varchar(100) DEFAULT NULL,
  `state` varchar(500) DEFAULT NULL,
  `country` varchar(500) DEFAULT NULL,
  `subtotal` decimal(50,0) DEFAULT NULL,
  `total` decimal(50,0) DEFAULT NULL COMMENT 'include postage fees',
  `payment_method` varchar(255) DEFAULT NULL,
  `payment_amount` int(255) DEFAULT NULL,
  `payment_bankreference` varchar(255) DEFAULT NULL,
  `payment_date` varchar(20) DEFAULT NULL,
  `payment_time` varchar(20) DEFAULT NULL,
  `payment_status` varchar(200) DEFAULT NULL COMMENT 'pending, accepted/completed, rejected, NULL = nothing',
  `shipping_status` varchar(200) DEFAULT NULL COMMENT 'pending, shipped, refunded',
  `shipping_method` varchar(200) DEFAULT NULL,
  `shipping_date` date DEFAULT NULL,
  `tracking_number` varchar(200) DEFAULT NULL,
  `reject_reason` varchar(255) DEFAULT NULL,
  `refund_method` varchar(255) DEFAULT NULL,
  `refund_amount` int(255) DEFAULT NULL,
  `refund_note` varchar(255) DEFAULT NULL,
  `refund_reason` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`id`, `uid`, `username`, `bank_name`, `bank_account_holder`, `bank_account_no`, `receipt`, `name`, `contactNo`, `email`, `address_line_1`, `address_line_2`, `address_line_3`, `city`, `zipcode`, `state`, `country`, `subtotal`, `total`, `payment_method`, `payment_amount`, `payment_bankreference`, `payment_date`, `payment_time`, `payment_status`, `shipping_status`, `shipping_method`, `shipping_date`, `tracking_number`, `reject_reason`, `refund_method`, `refund_amount`, `refund_note`, `refund_reason`, `date_created`, `date_updated`) VALUES
(2, 'eb3b449f0c4868acf7cebaa02887a3e6', 'testing', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-06 06:33:35', '2019-12-06 06:33:35'),
(3, '68a18ec9b977f0aa47a901fccdd666d4', 'aaaaaa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-06 08:10:05', '2019-12-06 08:10:05');

-- --------------------------------------------------------

--
-- Table structure for table `room_price`
--

CREATE TABLE `room_price` (
  `id` int(255) NOT NULL,
  `type_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `roomcapacity` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `display` smallint(1) NOT NULL DEFAULT 1,
  `price` decimal(50,2) NOT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `discount` int(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `room_price`
--

INSERT INTO `room_price` (`id`, `type_id`, `type`, `roomcapacity`, `name`, `display`, `price`, `duration`, `discount`, `description`) VALUES
(1, 1, 'Lounge', 1, 'Yearly Membership', 1, '1999.00', 'Month', 20, 'RM999 only if sign up on Nov 2019'),
(2, 1, 'Lounge', 1, 'Monthly Membership', 1, '199.00', 'Month', 20, 'RM99 only if sign up on Nov 2019\r\n'),
(3, 1, 'Lounge', 1, 'Daily Pass', 1, '30.00', 'Day', 20, 'RM20 only if sign up on Nov 2019\r\n'),
(4, 2, 'Work Desk', 1, 'Dedicated Work Desk', 1, '799.00', 'Month', 20, 'Enjoy 20% off from the monthly rental\r\n\r\n'),
(5, 2, 'Work Desk', 1, 'Co-Working Space (Hot Seat)', 1, '399.00', 'Month', 20, 'Enjoy 20% off from the monthly rental'),
(6, 3, 'Private Suit', 1, '1 Work Station', 1, '1000.00', 'Month', 20, 'Enjoy 20% off from the monthly rental'),
(7, 3, 'Private Suit', 2, '2 Work Station', 1, '1600.00', 'Month', 20, 'Enjoy 20% off from the monthly rental'),
(8, 3, 'Private Suit', 3, '3 Work Stations', 1, '2400.00', 'Month', 20, 'Enjoy 20% off from the monthly rental'),
(9, 3, 'Private Suit', 4, '4 Work Stations', 1, '3200.00', 'Month', 20, 'Enjoy 20% off from the monthly rental'),
(10, 3, 'Private Suit', 5, '5 Work Stations', 1, '4000.00', 'Month', 20, 'Enjoy 20% off from the monthly rental'),
(11, 3, 'Private Suit', 6, '6 Work Stations', 1, '4800.00', 'Month', 20, 'Enjoy 20% off from the monthly rental'),
(12, 3, 'Private Suit', 7, '7 Work Stations', 1, '5600.00', 'Month', 20, 'Enjoy 20% off from the monthly rental'),
(13, 3, 'Private Suit', 8, '8 Work Stations', 1, '6400.00', 'Month', 20, 'Enjoy 20% off from the monthly rental'),
(14, 4, 'Meeting Room', 0, 'Meeting Room', 1, '0.00', 'Hour', 20, 'Comfortable environment');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `uid` varchar(255) NOT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL COMMENT 'For login probably if needed',
  `email` varchar(200) DEFAULT NULL COMMENT 'Can login with email too',
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `ic_no` varchar(200) DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `login_type` int(2) NOT NULL DEFAULT 1 COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `birthday` date DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`uid`, `username`, `email`, `password`, `salt`, `phone_no`, `ic_no`, `full_name`, `country`, `login_type`, `user_type`, `date_created`, `date_updated`, `birthday`, `gender`) VALUES
('68a18ec9b977f0aa47a901fccdd666d4', 'aaaaaa', 'aa@gg.cc', 'd29a1fbf3d0e24f0a71c5f849d78cf66f71f4af086efda7ba968718072eeca53', 'ea9158d9a55b9404ef39423715c3245020fd1d9b', '111111', NULL, 'AAaa', 'Malaysia', 1, 1, '2019-11-01 03:45:33', '2019-12-05 04:20:53', NULL, NULL),
('afac5eac642581a6ae8523f7623de49a', 'lolo', 'lolo@gg.nm', '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '14141414', NULL, 'LOlo', 'Malaysia', 1, 1, '2019-11-01 05:01:54', '2019-12-05 04:50:43', NULL, NULL),
('c875c4116e8343e728db0a439b2097ee', 'admin', 'admin@gmail.com', '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '1233214455', NULL, 'ADmin', 'Malaysia', 1, 0, '2019-11-01 03:26:22', '2019-12-05 04:21:05', NULL, NULL),
('eb3b449f0c4868acf7cebaa02887a3e6', 'testing', 'test@gg.cc', '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '111222555', NULL, 'TEst', 'Singapore', 1, 1, '2019-10-31 03:51:51', '2019-12-05 04:21:11', NULL, NULL),
('f7c8cf4afec0dce4ccf677ac81661dec', 'zzzzz', 'zzz@gg.zz', '4f150972c7cef5240558a81dd6e136c13620defb6782a2fa05eeb71db0492ae6', 'a21926404c1d56d28829b210f76777c988b9f38b', '22222222', NULL, 'ZZZzzz', 'Singapore', 1, 1, '2019-10-31 09:33:15', '2019-12-05 04:21:15', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bookingUid_to_userUsername` (`uid`);

--
-- Indexes for table `booking_lounge`
--
ALTER TABLE `booking_lounge`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking_private`
--
ALTER TABLE `booking_private`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking_workdesk`
--
ALTER TABLE `booking_workdesk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room_price`
--
ALTER TABLE `room_price`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`uid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `booking_lounge`
--
ALTER TABLE `booking_lounge`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `booking_private`
--
ALTER TABLE `booking_private`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `booking_workdesk`
--
ALTER TABLE `booking_workdesk`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `room_price`
--
ALTER TABLE `room_price`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=419140;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `booking`
--
ALTER TABLE `booking`
  ADD CONSTRAINT `bookingUid_to_userUsername` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
