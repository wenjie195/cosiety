<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/RoomPrice.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$meetingRoom = 4;
$lounge = 1;
$privateSuit = 3;
$workDesk = 2;

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];


$loungeDetails = getRoomPrice($conn," WHERE type_id = ? and display = 1",array("type_id"),array($lounge),"i");
$privateSuitDetails = getRoomPrice($conn," WHERE type_id = ? and display = 1",array("type_id"),array($privateSuit),"i");
$workDeskDetails = getRoomPrice($conn," WHERE type_id = ? and display = 1",array("type_id"),array($workDesk),"i");
$meetingRoomDetails = getRoomPrice($conn," WHERE type_id = ? and display = 1",array("type_id"),array($meetingRoom),"i");


$conn->close();

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Add Booking | Cosiety" />
<title>Add Booking | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="grey-bg menu-distance2 same-padding overflow">
    <div class="divider"></div>
	<h1 class="backend-title-h1"><?php echo $loungeDetails[0]->getType() ?></h1>
    <div>

      <?php
   if($loungeDetails)
   {
     for ($cnt = 0;$cnt < count($loungeDetails) ;$cnt++) {



if($cnt % 2 == 0){ //odd check

    ?>

    <!--<form action="locationYear.php" method="POST">-->
    <form action="addBookingDetailsLounge.php" method="POST">
      <button class="two-box-div overflow hover-a booking-button" name="roomId" value="<?php echo $loungeDetails[$cnt]->getId() ?>">
                  <div class="color-header green-header">
                      <img src="img/meeting-room2.png" class="header-icon" alt="Yearly Membership" title="Yearly Membership"> <p><?php echo $loungeDetails[$cnt]->getName()  ?></p>
                  </div>
                  <div class="white-box-content booking-whitebox-content">
      				<p class="price-title"><b>RM<?php echo $loungeDetails[$cnt]->getPrice() ?>/</b><?php echo $loungeDetails[$cnt]->getDuration() ?></p>
                      <table class="details-table">
                      	<tr>
                          	<td>-</td>
                              <td> <?php echo $loungeDetails[$cnt]->getDescription() ?></td>
                          </tr>
                      </table>
                  </div>
              </button>
           </form>
         <?php }else {
           ?><form action="addBookingDetailsLounge.php" method="POST">
             <button class="two-box-div overflow second-box hover-a booking-button" name="roomId" value="<?php echo $loungeDetails[$cnt]->getId() ?>">
                         <div class="color-header violet-header">
                             <img src="img/meeting-room.png" class="header-icon" alt="Yearly Membership" title="Yearly Membership"> <p><?php echo $loungeDetails[$cnt]->getName()  ?></p>
                         </div>
                         <div class="white-box-content booking-whitebox-content">
             				<p class="price-title"><b>RM<?php echo $loungeDetails[$cnt]->getPrice() ?>/</b><?php echo $loungeDetails[$cnt]->getDuration() ?></p>
                             <table class="details-table">
                             	<tr>
                                 	<td>-</td>
                                     <td> <?php echo $loungeDetails[$cnt]->getDescription() ?></td>
                                 </tr>
                             </table>
                         </div>
                     </button>
                  </form><?php
         } ?>

   <?php }} ?>

    </div>

	<div class="clear"></div>
    <h1 class="backend-title-h1"><?php echo $workDeskDetails[0]->getType() ?></h1>

    <div>

      <?php
   if($workDeskDetails)
   {
     for ($cnt = 0;$cnt < count($workDeskDetails) ;$cnt++) {



if($cnt % 2 == 0){ //odd check

    ?>

    <!--<form action="locationYear.php" method="POST">-->
    <form action="addBookingDetails.php" method="POST">
      <button class="two-box-div overflow hover-a booking-button" name="roomId" value="<?php echo $workDeskDetails[$cnt]->getId() ?>">
        <div class="color-header red-header">
                <img src="img/seat2.png" class="header-icon" alt="Yearly Membership" title="Yearly Membership"> <p><?php echo $workDeskDetails[$cnt]->getName()  ?></p>
                  </div>
                  <div class="white-box-content booking-whitebox-content">
      				<p class="price-title"><b>RM<?php echo $workDeskDetails[$cnt]->getPrice() ?>/</b><?php echo $workDeskDetails[$cnt]->getDuration() ?></p>
                      <table class="details-table">
                      	<tr>
                          	<td>-</td>
                              <td> <?php echo $workDeskDetails[$cnt]->getDescription() ?></td>
                          </tr>
                      </table>
                  </div>
              </button>
           </form>
         <?php }else {
           ?><form action="addBookingDetails.php" method="POST">
             <button class="two-box-div overflow second-box hover-a booking-button" name="roomId" value="<?php echo $workDeskDetails[$cnt]->getId() ?>">
               <div class="color-header orange-header">
                   <img src="img/seat.png" class="header-icon" alt="Yearly Membership" title="Yearly Membership"> <p><?php echo $workDeskDetails[$cnt]->getName()  ?></p>
                         </div>
                         <div class="white-box-content booking-whitebox-content">
             				<p class="price-title"><b>RM<?php echo $workDeskDetails[$cnt]->getPrice() ?>/</b><?php echo $workDeskDetails[$cnt]->getDuration() ?></p>
                             <table class="details-table">
                             	<tr>
                                 	<td>-</td>
                                     <td> <?php echo $workDeskDetails[$cnt]->getDescription() ?></td>
                                 </tr>
                             </table>
                         </div>
                     </button>
                  </form><?php
         } ?>

   <?php }} ?>

    </div>
    <div class="clear"></div>


	<h1 class="backend-title-h1"><?php echo $privateSuitDetails[0]->getType() ?></h1>
  <div>

    <?php
 if($privateSuitDetails)
 {
   for ($cnt = 0;$cnt < count($privateSuitDetails) ;$cnt++) {



if($cnt % 2 == 0){ //odd check

  ?>

  <!--<form action="locationYear.php" method="POST">-->
  <form action="addBookingDetailsPrivate.php" method="POST">
    <button class="two-box-div overflow hover-a booking-button" name="roomId" value="<?php echo $privateSuitDetails[$cnt]->getId() ?>">
      <div class="color-header blue-header">
                <img src="img/group.png" class="header-icon" alt="Yearly Membership" title="Yearly Membership"> <p><?php echo $privateSuitDetails[$cnt]->getName()  ?></p>
                </div>
                <div class="white-box-content booking-whitebox-content">
            <p class="price-title"><b>RM<?php echo $privateSuitDetails[$cnt]->getPrice() ?>/</b><?php echo $privateSuitDetails[$cnt]->getDuration() ?></p>
                    <table class="details-table">
                      <tr>
                          <td>-</td>
                            <td> <?php echo $privateSuitDetails[$cnt]->getDescription() ?></td>
                        </tr>
                    </table>
                </div>
            </button>
         </form>
       <?php }else {
         ?><form action="addBookingDetailsPrivate.php" method="POST">
           <button class="two-box-div overflow second-box hover-a booking-button" name="roomId" value="<?php echo $privateSuitDetails[$cnt]->getId() ?>">
             <div class="color-header purple-header">
                <img src="img/group2.png" class="header-icon" alt="Yearly Membership" title="Yearly Membership"> <p><?php echo $privateSuitDetails[$cnt]->getName()  ?></p>
                       </div>
                       <div class="white-box-content booking-whitebox-content">
                  <p class="price-title"><b>RM<?php echo $privateSuitDetails[$cnt]->getPrice() ?>/</b><?php echo $privateSuitDetails[$cnt]->getDuration() ?></p>
                           <table class="details-table">
                            <tr>
                                <td>-</td>
                                   <td> <?php echo $privateSuitDetails[$cnt]->getDescription() ?></td>
                               </tr>
                           </table>
                       </div>
                   </button>
                </form><?php
       } ?>

 <?php }} ?>

  </div>
  <div class="clear"></div>
	<h1 class="backend-title-h1"><?php echo $meetingRoomDetails[0]->getType() ?></h1>
  <div>

    <?php
 if($meetingRoomDetails)
 {
   for ($cnt = 0;$cnt < count($meetingRoomDetails) ;$cnt++) {



if($cnt % 2 == 0){ //odd check

  ?>

  <!--<form action="locationYear.php" method="POST">-->
  <form action="addBookingDetails.php" method="POST">
    <button class="two-box-div overflow hover-a booking-button" name="roomId" value="<?php echo $meetingRoomDetails[$cnt]->getId() ?>">
      <div class="color-header gold-header">
                <img src="img/meeting-room3.png" class="header-icon" alt="Yearly Membership" title="Yearly Membership"> <p><?php echo $meetingRoomDetails[$cnt]->getName()  ?></p>
                </div>
                <div class="white-box-content booking-whitebox-content">
            <p class="price-title"><b>RM<?php echo $meetingRoomDetails[$cnt]->getPrice() ?>/</b><?php echo $meetingRoomDetails[$cnt]->getDuration() ?></p>
                    <table class="details-table">
                      <tr>
                          <td>-</td>
                            <td> <?php echo $meetingRoomDetails[$cnt]->getDescription() ?></td>
                        </tr>
                    </table>
                </div>
            </button>
         </form>
       <?php }else {
         ?><form action="addBookingDetails.php" method="POST">
           <button class="two-box-div overflow second-box hover-a booking-button" name="roomId" value="<?php echo $meetingRoomDetails[$cnt]->getId() ?>">
             <div class="color-header gold-header">
                <img src="img/meeting-room3.png" class="header-icon" alt="Yearly Membership" title="Yearly Membership"> <p><?php echo $meetingRoomDetails[$cnt]->getName()  ?></p>
                       </div>
                       <div class="white-box-content booking-whitebox-content">
                  <p class="price-title"><b>RM<?php echo $meetingRoomDetails[$cnt]->getPrice() ?>/</b><?php echo $meetingRoomDetails[$cnt]->getDuration() ?></p>
                           <table class="details-table">
                            <tr>
                                <td>-</td>
                                   <td> <?php echo $meetingRoomDetails[$cnt]->getDescription() ?></td>
                               </tr>
                           </table>
                       </div>
                   </button>
                </form><?php
       } ?>

 <?php }} ?>

  </div>


</div>
<?php include 'js.php'; ?>
</body>
</html>
