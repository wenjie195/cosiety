<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/RoomPrice.php';
require_once dirname(__FILE__) . '/../classes/BookingWorkDesk.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        $conn = connDB();
        //$uid = $_SESSION['uid'];

        //$id = md5(uniqid());
        //$display = rewrite($_POST['display']);
        //$type = rewrite($_POST['type']);
        $seat = rewrite($_POST['seat']);
        $user = rewrite($_POST['user']);
        //$discount = rewrite($_POST["discount"]);
        $price = rewrite($_POST['price']);
        $date = rewrite($_POST['date']);
        $duration = rewrite($_POST['duration']);
        //$description = rewrite($_POST['description']);
      }else {
        $id = uniqid();
        $type = "";
        $name = "";
        $display = 0;
        $price = "";
        $duration = "";
        $discount = "";
        $description = "";
      }

      if ($price) {
        $available = 1;
      }

$roomDetails = getWorkDesk($conn, "WHERE seat_id =?", array("seat_id"), array($seat), "i");
$seatDetails = $roomDetails[0]->getSeatID();

$userDetails = getUser($conn, "WHERE uid =?", array("uid"), array($uid), "s");
$userName = $userDetails[0]->getUsername();


      if(isset($_POST["updateButton"])){

        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if($available)
        {
            array_push($tableName,"seat_status");
            array_push($tableValue,$available);
            $stringType .=  "s";
        }
        if($price)
        {
            array_push($tableName,"payment_amount");
            array_push($tableValue,$price);
            $stringType .=  "i";
        }
        if($date)
        {
            array_push($tableName,"start_date");
            array_push($tableValue,$date);
            $stringType .=  "s";
        }
        if($duration)
        {
            array_push($tableName,"duration");
            array_push($tableValue,$duration);
            $stringType .=  "s";
        }
        if($userName)
        {
            array_push($tableName,"orderby");
            array_push($tableValue,$userName);
            $stringType .=  "s";
        }
        // if(!$display)
        // {   $display = 0;
        //     array_push($tableName,"display");
        //     array_push($tableValue,$display);
        //     $stringType .=  "i";
        // }
        // if($display)
        // {
        //     array_push($tableName,"display");
        //     array_push($tableValue,$display);
        //     $stringType .=  "i";
        // }

        array_push($tableValue,$seatDetails);
        $stringType .=  "i";
        $roomUpdate = updateDynamicData($conn,"booking_workdesk"," WHERE seat_id = ? ",$tableName,$tableValue,$stringType);
        if($roomUpdate)
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../addBooking.php?type=1');
        }
        else
        {
            echo "fail";

        }

      }



?>
