<div class="landing-first-div">
    <!-- Start Menu -->
    <header id="header" class="header header--fixed same-padding header1 menu-white" role="banner">
        <div class="big-container-size hidden-padding">
            <div class="left-logo-div float-left hidden-logo-padding">
                <img src="img/cosiety-logo.png" class="logo-img" alt="Cosiety" title="Cosiety">
            </div>
            
            <div class="right-menu-div float-right" id="top-menu">
            	
                <a href="adminManage.php" class="menu-padding">Manage</a>
                <a href="adminBooking.php" class="menu-padding">Booking</a>
                <a href="adminAnalyse.php" class="menu-padding">Analyse</a>
                <a href="adminCustomise.php" class="menu-padding">Customise</a>
                <div class="dropdown dropdown-1">
                  <span><img src="img/admin.png" class="menu-profile" alt="Profile" title="Profile"> <i class="fa fa-caret-down"></i></span>
                  <div class="dropdown-content">
                  	<a href="adminProfile.php">Profile</a>
                    <a href="editPassword.php">Edit Password</a>
                    <a href="index.php">Logout</a>
                  </div>
                </div>
                
                <a class="menu-icon openmenu"><img src="img/menu.png" class="menu-img" alt="Menu" title="menu"></a>

            </div>
        </div>
    
    </header>

</div>

<!--->
      	
                <a href="adminManage.php" class="menu-padding">Manage</a>
                <a href="adminBooking.php" class="menu-padding">Booking</a>
                <a href="adminAnalyse.php" class="menu-padding">Analyse</a>
                <a href="adminCustomise.php" class="menu-padding">Customise</a>
                <div class="dropdown dropdown-1">
                  <span><img src="img/admin.png" class="menu-profile" alt="Profile" title="Profile"></span>
                  <div class="dropdown-content">
                  	<a href="adminProfile.php">Profile</a>
                    <a href="editPassword.php">Edit Password</a>
                    <a href="index.php">Logout</a>
                  </div>
                </div>
<!-- The Modal -->
<div id="menumodal" class="modal modal-css">

  <!-- Modal content -->
  <p class="close-menu-p"><span class="closemenu close-css">&times;</span></p>
  <div class="clear"></div>
  <div class="modal-content modal-content-css">
  				<h2 class="mobile-menu-h2">Menu</h2>
                <a href="adminManage.php" class="mobile-menu-a">Manage</a>
                <a href="adminBooking.php" class="mobile-menu-a">Booking</a>
                <a href="adminAnalyse.php" class="mobile-menu-a">Analyse</a>
                <a href="adminCustomise.php"  class="mobile-menu-a">Customise</a>
                <a href="adminCustomise.php"  class="mobile-menu-a">Profile</a>
                <a href="adminProfile.php"  class="mobile-menu-a">Edit Password</a>
                <a href="index.php"  class="mobile-menu-a">Logout</a>    
    			<a class="mobile-menu-a closemenu last-menu-a">Close</a>  
  </div>

</div>