<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Notifications | Cosiety" />
<title>Notifications | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1">Notification</h1>

            <div class="white-box-content-noti">
            	<a href="paymentMethod.php" class="hover-effect">
                    
                        <div class="left-icon-div grey-icon hover-effect"><img src="img/bill.png" class="white-icon2 hover-effect" alt="Invoice" title="Invoice"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect">12/8/2019    10:00 am</p>
                            <p class="white-box-content-p hover-effect margin-bottom-0">Next Invoice: 12/9/2019 (RM 200.00)</p>
                        </div>
                  
                </a>
            </div>
            <div class="white-box-content-noti">
                <a href="addBooking.php" class="hover-effect">
               
                        <div class="left-icon-div grey-icon hover-effect"><img src="img/booking.png" class="white-icon2 hover-effect" alt="Booking" title="Booking"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect">12/8/2019    10:00 am</p>
                            <p class="white-box-content-p hover-effect margin-bottom-0">Booking for 1 month and get a 20% discount for Private Suit!</p>
                        </div>
               
                </a>
            </div>
            <div class="white-box-content-noti">
                <a href="addBooking.php" class="hover-effect">               
              
                        <div class="left-icon-div grey-icon hover-effect"><img src="img/calendar.png" class="white-icon2 hover-effect" alt="Calendar" title="Calendar"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect">12/8/2019    10:00 am</p>
                            <p class="white-box-content-p hover-effect margin-bottom-0">Your booking space will be expired on 12/8/2019 10:00 am</p>
                        </div>
                  
                </a>                     
            </div>

  
     
</div>


<?php include 'js.php'; ?>
</body>
</html>