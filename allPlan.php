<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="All Plan | Cosiety" />
<title>All Plan | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1 align-select-h1">All Plan <a href="addBooking.php" class="hover1"><img src="img/add.png" class="add-icon hover1a" alt="Add New Plan" title="Add New Plan"><img src="img/add2.png" class="add-icon hover1b" alt="Add New Plan" title="Add New Plan"></a></h1>
	<select class="clean align-h1-select">
    	<option>Latest</option>
        <option>Oldest</option>
        <option>Expired</option>
        <option>On Going</option>
    </select>
	<div class="clear"></div>
    <div class="width100">
    	<div class="overflow-scroll-div">    
            <table class="issue-table">
            	<tr>
                	<thead>
                    	<th>No.</th>
                        <th>Plan</th>
                        <th>Status</th>
                        <th>Expired</th>
                        <th>Paid On</th>
                        <th>Amount (RM)</th>
                    </thead>
                </tr>
                <tr data-url="receipt.php" class="link-to-details hover-effect">
                	<td>1.</td>
                    <td>Meeting Room E-11 - <b>Basic Plan E</b></td>
                    <td class="red-text">Expired</td>
                    <td>12/8/2019</td>
                    <td>12/7/2019</td>
                    <td>50.00</td>
                </tr>
                <tr data-url="receipt.php" class="link-to-details hover-effect">
                	<td>2.</td>
                    <td>Meeting Room E-11 - <b>Basic Plan E</b></td>
                    <td class="green-status">On Going</td>
                    <td>12/9/2019</td>
                    <td>12/8/2019</td>
                    <td>50.00</td>
                </tr>                
            </table>
		</div>
    </div>
  		<!--
        <div class="clear"></div>
        <div class="fillup-leftspace"></div><a href="addBooking.php"><div class="blue-btn add-new-btn">Add New Booking</div></a>-->
  
     
</div>


<?php include 'js.php'; ?>
</body>
</html>