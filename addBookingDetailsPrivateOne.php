<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Booking.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $area_type = rewrite($_POST["area_type"]);
    $cost = rewrite($_POST["cost"]);
}


$conn->close();

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Booking Details | Cosiety" />
<title>Booking Details | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
<form action="addBookingDetailsSeatPrivateOne.php" method="POST">
<!-- <form action="utilities/bookingFunction.php" method="POST"> -->
<!-- <h4> <?php //echo $userDetails->getUsername();?> </h4> -->
<!-- <h4> <?php //echo $area_type?> </h4> -->
<!-- <h4> <?php //echo $cost?> </h4> -->

    <!-- <h1 class="backend-title-h1">Co-Working Space (Hot Seat)</h1> -->
    <h1 class="backend-title-h1">Private Suit - Straits Quay</h1>

    <input type="hidden" name="area_type" id="area_type" value="<?php echo $area_type?>">
    <input type="hidden" name="cost" id="cost" value="<?php echo $cost?>">

	<div class="three-div">
    	<p class="grey-text input-top-p">Duration (month)</p>
        <input class="three-select clean" id="duration" name="duration"  type="number" placeholder="1" >
    </div>
	<div class="three-div middle-three-div second-three-div">
        <p class="grey-text input-top-p">Start Date</p>
        <input class="three-select clean" type="date" id="start_date" name="start_date">
        <!-- <input type="date" class="three-select clean"> -->
    </div>
	<div class="three-div">
    	<p class="grey-text input-top-p">Discount</p>
        <p class="three-select-p ow-margin-bottom-0">20%</p>
        <input type="hidden" name="discount" id="discount" value="20%">
    </div>
    <div class="tempo-three-clear"></div> 
 	<div class="three-div second-three-div">
    	<p class="grey-text input-top-p">Total</p>
        <p class="total-p">RM639.20</p>
        <input type="hidden" name="total_price" id="total_price" value="RM639.20">
    </div>
    <div class="clear"></div>
	<div class="divider"></div>
    <div class="divider"></div>
    <!-- <div class="fillup-extra-space"></div><a href="paymentMethod.php"><button class="blue-btn payment-button clean">Proceed to Payment</button></a> -->

    <div class="fillup-extra-space"></div>
    <!-- <button input type="submit" name="submit" value="Submit" class="blue-btn payment-button clean">Proceed to Payment</button> -->
    <button input type="submit" name="submit" value="Submit" class="blue-btn payment-button clean" >Next</button>

    <div class="clear"></div>
    
</form>
</div>


<?php include 'js.php'; ?>
</body>
</html>