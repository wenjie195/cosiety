<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Edit Plan Category | Cosiety" />
<title>Edit Plan Category | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1">Edit Plan Category</h1>
    <div class="width100 overflow">
    	<p class="grey-text input-top-p">Plan Category Name</p>
    	<input type="text" class="three-select clean" placeholder="Plan Category Name">
    </div>

    <div class="clear"></div>
    <div class="divider"></div> 
    <div class="width100 overflow">
        <div class="fillup-2-btn-space"></div>
        <a href="customisePlan.php"><button class="clean red-btn text-center payment-button clean next-btn view-plan-btn print-btn open-confirm"   >Delete</button></a>
        <a href="customisePlan.php"><button class="payment-button clean next-btn view-plan-btn blue-btn">Save</button></a>
        <div class="fillup-2-btn-space"></div>
    </div> 
    <div class="clear"></div>
    <div class="fillup-extra-space2"></div><a  onclick="goBack()" class="cancel-a hover-effect">Cancel</a>
</div>


<?php include 'js.php'; ?>
</body>
</html>