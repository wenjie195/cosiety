<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Reserve Working Space | Cosiety" />
<title>Reserve Working Space | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1">Register Member</h1>
	<div class="three-div">
    	<p class="grey-text input-top-p">Seat No.1</p>
        <select class="three-select clean">
        	<option>Pick a member</option>
            <option>Alicia</option>
            <option>Felicia</option>
        </select>
    </div>
	<div class="three-div middle-three-div second-three-div">
    	<p class="grey-text input-top-p">Seat No.2</p>
        <select class="three-select clean">
        	<option>Pick a member</option>
            <option>Alicia</option>
            <option>Felicia</option>                                                        
        </select>
    </div>
	<div class="three-div">
    	<p class="grey-text input-top-p">Seat No.3</p>
        <select class="three-select clean">
        	<option>Pick a member</option>
            <option>Alicia</option>
            <option>Felicia</option>                                                        
        </select>
    </div>
    <div class="tempo-three-clear"></div> 
 	<div class="three-div second-three-div">
    	<p class="grey-text input-top-p">Seat No.4</p>
        <select class="three-select clean">
        	<option>Pick a member</option>
            <option>Alicia</option>
            <option>Felicia</option>                                                        
        </select>
    </div>
	<div class="three-div middle-three-div">
    	<p class="grey-text input-top-p">Seat No.5</p>
        <select class="three-select clean">
        	<option>Pick a member</option>
            <option>Alicia</option>
            <option>Felicia</option>                                                        
        </select>
    </div>
    <div class="clear"></div>    
	<div class="divider"></div>
    <div class="clear"></div>
    <div class="fillup-extra-space"></div><a href="receipt.php"><button class="blue-btn payment-button clean next-btn">Next</button></a>
    <div class="clear"></div>
    <div class="fillup-extra-space2"></div><a  onclick="goBack()" class="cancel-a hover-effect">Cancel</a>
</div>


<?php include 'js.php'; ?>
</body>
</html>