<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Booking.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$userBooking = getBooking($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userBookingDetails = $userBooking[0];

$conn->close();

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Dashboard | Cosiety" />
<title>Dashboard | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
    <h1 class="backend-title-h1">Dashboard</h1>
    
    <h4>Welcome, <?php echo $userDetails->getUsername();?> </h4>

    <div class="two-box-container">
        <div class="two-box-div overflow">
            <div class="color-header red-header">
                <img src="img/notification.png" class="header-icon" alt="Notification/News" title="Notification/News"> <p>Notification/News</p>
                <a href="" class="hover-effect white-text view-a">View All</a>
            </div>
            <div class="white-box-content">
            	                    
            </div>
        </div>
        <div class="two-box-div overflow second-box">
            <div class="color-header orange-header">
                <img src="img/booking.png" class="header-icon" alt="Booking" title="Booking"> <p>Upcoming Booking</p>
                <a href="booking.php" class="hover-effect white-text view-a">View All</a>
            </div>
            <div class="white-box-content">
            	<a href="receipt.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/seat.png" class="white-icon2 hover-effect" alt="Co-Working Space - No.1" title="Co-Working Space - No.1"></div>
                        <div class="right-icon-div">
                            <!-- <p class="light-grey-text small-date hover-effect">Today    10:00 am - Today 6:00 pm</p>
                            <p class="white-box-content-p hover-effect">Co-Working Space - No.1</p> -->

                            <!-- <p class="light-grey-text small-date hover-effect"><?php //echo $userBookingDetails->getBookingId();?></p>
                            <p class="white-box-content-p hover-effect"><?php //echo $userBookingDetails->getAreaType();?></p>
                            <p class="white-box-content-p hover-effect"><?php //echo $userBookingDetails->getStartDate();?> | <?php echo $userBookingDetails->getDuration();?></p> -->

                        </div>
                    </div>
                </a>
                         
            </div>            
        </div> 
    </div>  

        <!-- Add class booking for booking day inside the div class="day" inside calendar.js-->

        <div class="two-box-div overflow image-container">  
            <!-- Crop image into 16:9, preset the booking details to the plan and discount details. -->
            <a href="addBookingDetails.php" class="hover-effect"><img src="img/promotion.jpg" class="width100" alt="Promotion" title="Promotion"></a>
        </div>  
  
     
</div>


<?php include 'js.php'; ?>
</body>
</html>