<?php
class Workdesk {
    /* Member variables */
    var $id,$seat_id,$seat_status,$start_date,$duration,$end_date,$payment_amount,$payment_verify,$orderBy,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

     /**
     * @return mixed
     */
    public function getSeatID()
    {
        return $this->seat_id;
    }

    /**
     * @param mixed $seat_id
     */
    public function setSeatID($seat_id)
    {
        $this->seat_id = $seat_id;
    }

    /**
     * @return mixed
     */
    public function getSeatStatus()
    {
        return $this->seat_status;
    }

    /**
     * @param mixed $seat_status
     */
    public function setSeatStatus($seat_status)
    {
        $this->seat_status = $seat_status;
    }
  
/**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * @param mixed $start_date
     */
    public function setStartDate($start_date)
    {
        $this->start_date = $start_date;
    }

    /**
     * @return mixed
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param mixed $duration
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->end_date;
    }

    /**
     * @param mixed $end_date
     */
    public function setEndDate($end_date)
    {
        $this->end_date = $end_date;
    }

    /**
     * @return mixed
     */
    public function getPaymentAmount()
    {
        return $this->payment_amount;
    }

    /**
     * @param mixed $payment_amount
     */
    public function setPaymentAmount($payment_amount)
    {
        $this->payment_amount = $payment_amount;
    }

    /**
     * @return mixed
     */
    public function getPaymentVerify()
    {
        return $this->payment_verify;
    }

    /**
     * @param mixed $payment_verify
     */
    public function setPaymentVerify($payment_verify)
    {
        $this->payment_verify = $payment_verify;
    }

    /**
     * @return mixed
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * @param mixed $orderBy
     */
    public function setOrderBy($orderBy)
    {
        $this->orderBy = $orderBy;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

// function getBooking($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
//     $dbColumnNames = array("id","uid","booking_id","area_type","cost","duration","start_date","total_ppl","order_by",
//                 "discount","total_price","total_seat","project_title","project_details","payment_method","date_created","date_updated");

function getWorkDesk($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","seat_id","seat_status","start_date","duration","end_date","payment_amount","payment_verify",
            "orderBy","dateCreated","dateUpdated");


    $sql = sqlSelectSimpleBuilder($dbColumnNames,"booking_workdesk");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$seat_id,$seat_status,$start_date,$duration,$end_date,$payment_amount,$payment_verify,
                            $orderBy,$dateCreated,$dateUpdated);


        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Workdesk();

            $class->setId($id);
            $class->setSeatID($seat_id);
            $class->setSeatStatus($seat_status);
            $class->setStartDate($start_date);
            $class->setDuration($duration);
            $class->setEndDate($end_date);
            $class->setPaymentAmount($payment_amount);
            $class->setPaymentVerify($payment_verify);
            $class->setOrderBy($orderBy);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}