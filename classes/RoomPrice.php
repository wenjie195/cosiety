<?php
class RoomPrice {
    /* Member variables */
    //var $id,$uid,$address_line_1,$address_line_2,$address_line_3,$city,$zipcode,$state,$country,$dateCreated,$dateUpdated;
    var $id,$typeId,$type,$roomCapacity,$name,$price,$discount,$duration,$description,$display;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTypeId()
    {
        return $this->typeId;
    }

    /**
     * @param mixed $uid
     */
    public function setTypeId($typeId)
    {
        $this->typeId = $typeId;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $uid
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getRoomCapacity()
    {
        return $this->roomCapacity;
    }

    /**
     * @param mixed $roomCapacity
     */
    public function setRoomCapacity($roomCapacity)
    {
        $this->roomCapacity = $roomCapacity;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $bookingId
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $areaType
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param mixed $cost
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * @return mixed
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param mixed $cost
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $cost
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getDisplay()
    {
        return $this->display;
    }

    /**
     * @param mixed $cost
     */
    public function setDisplay($display)
    {
        $this->display = $display;
    }

}

function getRoomPrice($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    // $dbColumnNames = array("id","type_id","type","name","price","discount","duration","description","display");
    $dbColumnNames = array("id","type_id","type","roomcapacity","name","price","discount","duration","description","display");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"room_price");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        // $stmt->bind_result($id,$typeId,$type,$name,$price,$discount,$duration,$description,$display);
        $stmt->bind_result($id,$typeId,$type,$roomCapacity,$name,$price,$discount,$duration,$description,$display);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new RoomPrice();
            $class->setId($id);
            $class->setTypeId($typeId);
            $class->setType($type);
            $class->setRoomCapacity($roomCapacity);
            $class->setName($name);
            $class->setPrice($price);
            $class->setDiscount($discount);
            $class->setDuration($duration);
            $class->setDescription($description);
            $class->setDisplay($display);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
