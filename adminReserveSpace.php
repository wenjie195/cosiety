<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Reserve Working Space | Cosiety" />
<title>Reserve Working Space | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1">Reserve Working Space</h1>
	<div class="edit-half-div">
    	<p class="grey-text input-top-p">Working Space</p>
        <select class="three-select clean">
        	<option>1</option>
            <option>2</option>
        </select>
	</div>
	<div class="edit-half-div second-edit-half-div">
    	<p class="grey-text input-top-p">Member</p>
        <select class="three-select clean">
        	<option>Alicia Tang</option>
            <option>Jack Lim</option>
        </select>
	</div>            
	<div class="clear"></div>
    <div class="edit-half-div">
    	<p class="grey-text input-top-p">Working Space</p>
        <select class="three-select clean">
        	<option>1</option>
            <option>2</option>
        </select>
	</div>
	<div class="edit-half-div second-edit-half-div">
    	<p class="grey-text input-top-p">Member</p>
        <select class="three-select clean">
        	<option>Alicia Tang</option>
            <option>Jack Lim</option>
        </select>
	</div>            
	<div class="clear"></div>
    <div class="edit-half-div">    
     	<p class="grey-text input-top-p">Working Space</p>
        <select class="three-select clean">
        	<option>1</option>
            <option>2</option>
        </select>
	</div>
	<div class="edit-half-div second-edit-half-div">
    	<p class="grey-text input-top-p">Member</p>
        <select class="three-select clean">
        	<option>Alicia Tang</option>
            <option>Jack Lim</option>
        </select>
	</div>     
    <div class="clear"></div> 
    <div class="edit-half-div">    
     	<p class="grey-text input-top-p">Working Space</p>
        <select class="three-select clean">
        	<option>1</option>
            <option>2</option>
        </select>
	</div>
	<div class="edit-half-div second-edit-half-div">
    	<p class="grey-text input-top-p">Member</p>
        <select class="three-select clean">
        	<option>Alicia Tang</option>
            <option>Jack Lim</option>
        </select>
	</div>     
    <div class="clear"></div> 
    <div class="edit-half-div">    
     	<p class="grey-text input-top-p">Working Space</p>
        <select class="three-select clean">
        	<option>1</option>
            <option>2</option>
        </select>
	</div>
	<div class="edit-half-div second-edit-half-div">
    	<p class="grey-text input-top-p">Member</p>
        <select class="three-select clean">
        	<option>Alicia Tang</option>
            <option>Jack Lim</option>
        </select>
	</div>     
    <div class="clear"></div>              
	<div class="divider"></div>
    <div class="clear"></div>
	<div class="width100 overflow">
	<div class="fillup-2-btn-space"></div>
	<button class="clean print-btn"  onclick="goBack()">Cancel</button>
	<div class="blue-btn payment-button clean next-btn view-plan-btn">Confirm</div></a>
	<div class="fillup-2-btn-space"></div>
	</div>
	<div class="clear"></div>
</div>


<?php include 'js.php'; ?>
</body>
</html>