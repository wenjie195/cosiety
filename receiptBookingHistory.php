<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Booking.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
// $userDetails = $userRows[0];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{

}

$conn->close();

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Receipt | Cosiety" />
<title>Receipt | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">

    <h1 class="receipt-title-h1">Receipt</h1> <span class="logo-span"><img src="img/cosiety-logo.png" class="logo-img2" alt="Cosiety" title="Cosiety"></span>
    
    <div class="clear"></div>

    <div class="address-div">
        <p class="receipt-address-p"><b class="black-text">Cosiety Sdn. Bhd.</b><br>No, Street,<br>Town, Postcode,<br>City, State,<br>Country</p>
    </div>

    <div class="clear"></div>

    <?php
    if(isset($_POST['bookingHistory_ID']))
    {
        $conn = connDB();
        $bookingReceipt = getBooking($conn,"WHERE booking_id = ? ", array("booking_id") ,array($_POST['bookingHistory_ID']),"s");

        $searchUid = $bookingReceipt[0]->getUid();

        $userDetails = getUser($conn,"WHERE uid = ? ", array("uid") ,array($searchUid),"s");
        // $aaa = $userDetails[0]->getFullName();

            if($bookingReceipt != null)
            {
            ?>

            <div class="receipt-half-div">
                <h2 class="receipt-subtitle-h2 black-text">ISSUED TO</h2>
            </div>

            <div class="receipt-half-div second-receipt-half-div">
                <h2 class="receipt-subtitle-h2 align-p-h2 black-text">Receipt No.</h2>
                <!-- <p class="align-h2-p">112003</p> -->
                <p class="align-h2-p">
                    <?php $dateCreated = date("Ymd",strtotime($bookingReceipt[0]->getStartDate()));echo $dateCreated;?><?php echo $bookingReceipt[0]->getId(); ?>
                </p>
            </div>      
                
            <div class="clear"></div>

            <div class="receipt-half-div">
                <p class="receipt-address-p no-margin-top">
                    <b class="black-text">Company Name</b>
               
                    <br><?php echo $userDetails[0]->getFullName();; ?><br>
                    <!-- <br>No, Street,<br>Town, Postcode,<br>City, State,<br>Country -->
                </p>
            </div>        

            <div class="receipt-half-div second-receipt-half-div">
            	<h2 class="receipt-subtitle-h2 align-p-h2 no-margin-top black-text">Issue Date</h2>
                <!-- <p class="align-h2-p no-margin-top">12/8/2019</p> -->
                <p class="align-h2-p no-margin-top">
                    <?php $dateCreated = date("d/m/Y",strtotime($bookingReceipt[0]->getDateCreated()));echo $dateCreated;?>
                </p>
            </div>  

            <div class="clear"></div>      

            <div class="width100 receipt-border"></div>      

            <div class="clear"></div>
            <div class="receipt-half-div">
            	<p class="receipt-upper-p">Plan<br>
                <b class="receipt-lower-p"><?php echo $bookingReceipt[0]->getAreaType(); ?></b></p>
            </div>     

            <div class="receipt-half-div second-receipt-half-div">
            	<p class="receipt-upper-p">Duration<br>
                <b class="receipt-lower-p"><?php echo $bookingReceipt[0]->getDuration(); ?> Months</b></p>
            </div> 

            <div class="clear"></div>  
            <div class="receipt-half-div">
                <p class="receipt-upper-p">Start Date<br>
                <!-- <b class="receipt-lower-p">1/9/2019</b></p> -->
                <b class="receipt-lower-p">
                    <?php $dateCreatedST = date("d/m/Y",strtotime($bookingReceipt[0]->getStartDate()));echo $dateCreatedST;?>
                </b>
                </p>
            </div>   

            <div class="receipt-half-div second-receipt-half-div">
            	<p class="receipt-upper-p">End Date<br>
                <!-- <b class="receipt-lower-p">1/10/2019</b></p> -->
                <b class="receipt-lower-p">
                    <?php $dateCreatedEND = date("d/m/Y",strtotime($bookingReceipt[0]->getEndDate()));echo $dateCreatedEND;?>
                </b>
            </div> 
            
            <div class="clear"></div> 
            <div class="receipt-half-div">
                <p class="receipt-upper-p">Reserved Working Space<br>
                <b class="receipt-lower-p"><?php echo $bookingReceipt[0]->getTotalSeat(); ?></b></p>
            </div> 

            <div class="clear"></div> 

            <div class="width100 receipt-border"></div>               
            <div class="overflow width100 total-container">
            	<div class="receipt-left-total">Payment Method</div>
                <!-- <div class="receipt-right-total">iPay88</div> -->
                <div class="receipt-right-total"><?php echo $bookingReceipt[0]->getPaymentMethod(); ?></div>
            </div>

            <div class="clear"></div>     

            <div class="overflow width100 total-container">
            	<div class="receipt-left-total">Subtotal</div>
                <!-- <div class="receipt-right-total">RM957.60</div> -->
                <div class="receipt-right-total"><?php echo $bookingReceipt[0]->getCost(); ?></div>
            </div>

            <div class="clear"></div>      

            <div class="overflow width100 total-container">
            	<div class="receipt-left-total">Discount</div>
                <!-- <div class="receipt-right-total slight-left">- </div> -->
                <!-- <div class="receipt-right-total slight-left"><?php //echo $bookingReceipt[0]->getDiscount(); ?></div> -->
                <div class="receipt-right-total"><?php echo $bookingReceipt[0]->getDiscount(); ?>%</div>
            </div>

            <div class="clear"></div> 

            <div class="overflow width100 total-container padding-bottom-0">
            	<div class="receipt-left-total bigger-font">Total</div>
                <!-- <div class="receipt-right-total bigger-font">RM957.60</div> -->
                <div class="receipt-right-total bigger-font"><?php echo $bookingReceipt[0]->getTotalPrice(); ?></div>
            </div>

            <div class="clear"></div> 

            <div class="width100 receipt-border"></div>             
            <div class="clear"></div>                                                                                                                    
            <div class="divider"></div>
            <div class="clear"></div>

            <div class="width100 overflow receipt-two-btn-container">
            	<div class="fillup-2-btn-space"></div>
                <button class="clean print-btn"  onclick="printReceipt()">Print</button>
            	<a href="viewPlan.php"><div class="blue-btn payment-button clean next-btn view-plan-btn">View Plan</div></a>
                <div class="fillup-2-btn-space"></div>
            </div>
            <div class="clear"></div>

            <?php
            }
        $conn->close();
    }
    else
    {
        ?>
            <p>No data found...</p>
        <?php
    }
    ?>
        
</div>

<?php include 'js.php'; ?>
</body>
</html>